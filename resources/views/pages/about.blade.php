@extends('layout.app')
@section('content')
    <!-- page header -->
    <section class="page-head">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header-title">
                        About Us
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page header -->

    <!--About Us Start-->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <!-- aboutus content -->
                <div class="col-md-7 col-sm-12">
                    <div class="section-title left">
                        <span>About Us</span>
                        <h1></h1>
                    </div>
                    <div class="about-contant">
                        <p>Welcome to the new Airport Executive Website - Airport Transfers & Chauffeur Driven Cars in Aylesbury Airport Transfers Aylesbury We have been providing executive airport transfers and chauffeur driven cars in Aylesbury for over 15 years and are one of the Aylesbury's leading private hire firms. Whether you are coming to Aylesbury on business or pleasure, by yourself or with family and friends, we have a car to suit your needs. Our executive saloons provide the ultimate in comfort and class while our MPV accommodate whole families and small parties.</p>
                        <p>Our services include airport transfers, single trips and personal chauffeur driven cars. We also arrange private London & Oxford tours so that you can get to experience the best of London in superior comfort.</p>
                    </div>
                </div>
                <!-- end aboutus content -->

                <!-- about right image -->
                <div class="col-md-5 col-sm-12 wow fadeIn">
                    <div class="about-right-side">
                        <img class="img-responsive" src="{{('arc/images/about-us.jpg')}}" alt="about-side">
                    </div>
                </div>
                <!-- about right image -->
            </div>
        </div>
    </section>
    <!--About End-->
 

<section class="section bg-grey" id="blog">
        <div class="container">
            <div class="row">
                <div class="section-title text-center">
                    <span>Choose Us</span>
                    <h1>We are providing alot services</h1>
                </div>
            </div>
            <div class="row conatnt-row">

                <!-- blog item -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="featured-blog-box">
                        <div class="featured-img">
                            <img alt="" src="{{asset('arc/images/licensed-professional.png')}}">
                            <div class="featured-lable-blog">
                                <p class="admin-lable"></p>
                                <p class="blog-date"></p>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <a href="#" class="featured-data">
                            <div class="blog-title">LICENSED PROFESSIONALS</div>
                            <div class="blog-description">We take only those people on board for a job who have a clear history. Our drivers policy checked from the criminal records bureau. You are safe to travel with us.</div>
                        </a>
                    </div>
                </div>
                <!-- end blog item -->

                <!-- blog item -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="featured-blog-box">
                        <div class="featured-img">
                            <img alt="" src="{{asset('arc/images/economical.png')}}">
                            <div class="featured-lable-blog">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <a href="#" class="featured-data">
                            <div class="blog-title">WE ARE ECONOMICAL</div>
                            <div class="blog-description">If you don't have much budget with you but you want a luxurious car and service then you can hire us without any doubt.For us customers are king and we treat them accordingly. </div>
                        </a>
                    </div>
                </div>
                <!-- end blog item -->

                <!-- blog item -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="featured-blog-box last">
                        <div class="featured-img">
                            <img alt="" src="{{asset('arc/images/pament-option.png')}}">
                            <div class="featured-lable-blog">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <a href="#" class="featured-data">
                            <div class="blog-title">PAYMENT OPTION</div>
                            <div class="blog-description">We Support different payment options and these payment options range from PayPal, visa, Amex and MasterCard.</div>
                        </a>
                    </div>
                </div>
                <!-- end blog item -->

            </div>
            <div class="row conatnt-row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="featured-blog-box last">
                        <div class="featured-img">
                            <img alt="" src="{{asset('arc/images/privacy-policy.png')}}">
                            <div class="featured-lable-blog">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <a href="#" class="featured-data">
                            <div class="blog-title">PRIVACY POLICY</div>
                            <div class="blog-description">Arc executive keep the our customers information   confidential. We will never slip your information to any third party and we won't discuss anything about your credit card. We are reliable and that is why we are loved.</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end feature section --> 

    <!--Fleet Start-->
    @include('pages.fleet');
    <!--Fleet Start-->

   @endsection