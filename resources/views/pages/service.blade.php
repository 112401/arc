@extends('layout.app')
@section('content')
    <!-- page header -->
    <section class="page-head">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header-title">
                        Services
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page header -->
    <section class="section" id="service"  style="background: #f1f1f1 !important">
        <div class="container">
            <div class="row">
                <!-- aboutus content -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title left">
                        <h1>EXCEPTIONAL TRAVEL SERVICES</h1>
                    </div>
                    <div class="about-contant">
                        <p>Welcome to the new Airport Executive Website - Airport Transfers & Chauffeur Driven Cars in Aylesbury Airport Transfers Aylesbury We have been providing executive airport transfers and chauffeur driven cars in Aylesbury for over 15 years and are one of the Aylesbury's leading private hire firms. Whether you are coming to Aylesbury on business or pleasure, by yourself or with family and friends, we have a car to suit your needs. Our executive saloons provide the ultimate in comfort and class while our MPV accommodate whole families and small parties.</p>
                        <p>Our services include airport transfers, single trips and personal chauffeur driven cars. We also arrange private London & Oxford tours so that you can get to experience the best of London in superior comfort.</p>
                    </div>
                </div>
                <!-- end aboutus content -->
            </div>
        </div>
    </section>

    <!--Services Start-->
    <section class="section bg-grey" id="services">
        <div class="container">
            <div class="row">
                <div class="section-title text-center">
                    <span>Our Services</span>
                    <h1>We Provide World Class Services</h1>
                </div>
            </div>
            <div class="row conatnt-row">
                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp">
                    <div class="service-box-1">
                        <span><i class="fa fa-plane"></i></span>
                        <a href="{{route('arcAirport')}}">AIRPORT</a>
                        <p>we know how exhausting and fatiguing a journey in a plane can be...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="service-box-1">
                        <span><i class="fa fa-building-o"></i></span>
                        <a href="{{route('arcCorporate')}}">CORPORATE</a>
                        <p>Finding a ride that is in accord to your needs and specifications can be a difficult...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="service-box-1">
                        <span><i class="fa fa-truck"></i></span>
                        <a href="{{route('arcCourier')}}">COURIER SERVICES</a>
                        <p>ARC Executive is proud to familairize their clients with their competitive courier services...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="service-box-1">
                        <span><i class="fa fa-handshake-o"></i></span>
                        <a href="{{route('arcMeetGreet')}}">MEET AND GREET</a>
                        <p>ARC Executive provides optimum and competetive meet and greet services in town...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="service-box-1">
                        <span><i class="fa fa-futbol-o"></i></span>
                        <a href="{{route('arcSportEvents')}}">SOCIAL AND SPORTING EVENTS</a>
                        <p>We ensure you a safe ride. We make sure you reach the destination safe and sound...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="service-box-1">
                        <span><i class="fa fa-tripadvisor"></i></span>
                        <a href="{{route('arcLondonTour')}}">LONDON TOURS</a>
                        <p>Bet on ARC Executive for a hassle free journey and revel in your trip to it's fullest...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="service-box-1">
                        <span><i class="fa fa-tasks"></i></span>
                        <a href="{{route('arcDayHire')}}">DAY HIRE </a>
                        <p> Our day hire service ables you to use our luscious car services for all day long...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.7s">
                    <div class="service-box-1 last">
                        <span><i class="fa fa-heart"></i></span>
                        <a href="{{route('arcWedding')}}">WEDDING SERVICES</a>
                        <p>The soon-to-be bride and groom will love our wedding limo services will enjoy our coordinated wedding transportation...</p>
                    </div>
                </div>
                <!-- end service-box -->
            </div>
        </div>
    </section>
    <!--Services End-->

    <!-- services start -->
    <section class="section bg-grey" id="Testimonial">
        <div class="container">
            <div class="col-xs-12 col-md-4 col-lg-4  wow bounceInRight" data-wow-duration="1.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.5s;">
                <img src="http://www.arcexecutive.co.uk/public/images/support.jpg" class="img-responsive support1" alt="support">
                <h3 class="feat">Licensed professionals</h3>
                <p class="excellente">
                    We make sure that we take only those people on board for a job who have a clear history. Our chauffeurs have never been at jail neither they have performed any offence. Our drivers are policy checked from the criminal records bureau. You are safe to travel with us.</p>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4 wow bounceInUp" data-wow-duration="1.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.5s;">
                <img src="http://www.arcexecutive.co.uk/public/images/team.png" class="img-responsive support2" alt="team">
                <h3 class="feat">Payment option</h3>
                <p class="excellente">
                    We Support different payment options and these payment options range from PayPal, visa, Amex and MasterCard.
                </p>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4  wow bounceInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.5s;">
                <img src="http://www.arcexecutive.co.uk/public/images/fast.png" class="img-responsive support3" alt="fast">
                <h3 class="feat">Privacy Policy</h3>
                <p class="excellente">
                    The best thing about Arcexecutive is we keep the information about our customers confidential. We will never slip your information to any third party and we won't discuss anything about your credit card. We are reliable and that is why we are loved.
                </p>
            </div>
        </div>
    </section>
    <!-- services end -->
@endsection