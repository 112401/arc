@extends('layout.app')
@section('content')

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUeFGwxXtr8M8SD82BbA-6857CVPKUhNo&callback=initMap"
  type="text/javascript"></script>
     
    <section class="show-message">
        <div class="w3-container">
            @if ($message = Session::get('success'))
                <div class="w3-panel w3-green w3-display-container">
                    <div class="col-md-8 col-md-offset-3">
                        <p class="alert alert-success" style="margin-top:40px; height: 80px;text-align: center">{{$message}}
                            <button type="button" class="close" data-dismiss="alert">&times</button>
                        </p>
                    </div>
                </div>
                <?php Session::forget('success');?>
            @endif
            @if ($message = Session::get('error'))
                <div class="w3-panel w3-green w3-display-container">
                    <div class="col-md-8 col-md-offset-3">
                        <p class="alert alert-danger" style="margin-top: -125px;">You can not proceed this process contact with Arc Executive</p>
                    </div>
                </div>
                <?php Session::forget('error');?>
            @endif
            @if(Session::has('message'))
                <div class="w3-panel w3-green w3-display-container" role="alert">
                    <div class="col-md-8 col-md-offset-3">
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">&times</button>
                            <strong>Warning ! {{ Session::get('message') }}</strong>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
    
    <section class="page-head">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header-title">
                        Book Now
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="map">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="googleMap" style="height: 400px;display: none"> 
                    </div>
                </div>
            </div>
        </div>        
    </section>

    <section class="booking-form">
        <div class="col-md-10 col-md-offset-1" style="position: relative;margin-bottom: 20px">
            <div class="col-md-12 col-sm-12 col-xs-12" style="background: white;border:2px solid #30b9ee;padding: 20px;border-radius: 10px">
                <form method="POST" id="payment-form"  action="{{route('paypal10')}}" id="myForm">
                @csrf
                    <input required  class="w3-input w3-border" name="amount" id="myText1" type="hidden">

                <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;">
                    <h4 style="color:white;margin: 0px">1 Choose your journey</h4>
                </div>


                    <div class="col-md-12">
                        <div class="form-group col-md-4" style="padding-top: 1%;">
                            <div class="col-md-3">
                                <label style="border-radius: 10px !important;" for="name">From:</label>
                            </div>
                            <div class="col-md-9" style="border-radius: 30px !important;">
                                <select name="pickairport" id="booking_place" class="form-control dynamic" data-dependent="airport" style="border-radius: 10px !important;">
                                    <option value="" selected="selected">--Select--</option>
                                    <option value="from_town">Town/Village</option>
                                    <option value="from_airport">Airport</option>
                                    <option value="from_trainstation">Train Station</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4"  style="padding-top: 1%; display: none; border-radius: 10px !important" id="booking_place_show">
                            <div class="col-md-1"> </div>

                            <div class="col-md-9" id="booking_trainstation" style="display: none; border-radius: 10px !important;" >
                                <select style="border-radius: 10px !important;" name="booking_trainstation1" id="booking_trainstation_val" class="form-control dynamic" >
                                    <option selected="selected" value="">--Select--</option>
                                    <option value="Kings Cross Station">Kings Cross Station</option>
                                    <option value="Marylebone Station">Marylebone Station</option>
                                    <option value="St Pancras Station">St Pancras Station</option>
                                </select>
                            </div>

                            <div class="col-md-9" id="booking_airport" style="display: none; border-radius: 10px !important;" >
                            <select style="border-radius: 10px !important;" name="booking_airport1" id="booking_airport_val" class="form-control dynamic">
                                <option selected="selected" value="">--Select--</option>
                                <option value="Heathrow Airport Terminal 2">Heathrow Airport Terminal 2</option>
                                <option value="Heathrow Airport Terminal 3">Heathrow Airport Terminal 3</option>
                                <option value="Heathrow Airport Terminal 3">Heathrow Airport Terminal 4</option>
                                <option value="Heathrow Airport Terminal 5">Heathrow Airport Terminal 5</option>
                                <option value="Gatwick Airport South Terminal">Gatwick Airport South Terminal</option>
                                <option value="Gatwick Airport North Terminal">Gatwick Airport North Terminal</option>
                                <option value="Stansted Airport">Stansted Airport</option>
                                <option value="East Midlands Airport">East Midlands Airport</option>
                                <option value="London City Airport">London City Airport</option>
                                <option value="Luton Airport">Luton Airport</option>
                                <option value="Birmingham Airport">Birmingham Airport</option>
                                <option value="Manchester Airport">Manchester Airport</option>
                                <option value="Bristol">Bristol</option>
                            </select>
                            </div>

                            <div class="col-md-9" id="booking_town" style="display: none; border-radius: 10px; ">
                                <select style="border-radius: 10px !important;" name="booking_town1" id="booking_town_val" class="form-control dynamic" data-dependent="airport">
                                    <option selected="selected" value="">--Select--</option>
                                    <option value="Aylesbury">Aylesbury</option>
                                    <option value="Aston Abbotts">Aston Abbotts</option>
                                    <option value="Aston Clinton">Aston Clinton</option>
                                    <option value="Bicester">Bicester</option>
                                    <option value="Bishopstone">Bishopstone</option>
                                    <option value="Buckingham">Buckingham</option>
                                    <option value="Bierton">Bierton</option>
                                    <option value="Calvert">Calvert</option>
                                    <option value="Chearsley">Chearsley</option>
                                    <option value="Cheddington">Cheddington</option>
                                    <option value="Chilton">Chilton</option>
                                    <option value="Chinnor">Chinnor</option>
                                    <option value="Cuddington">Cuddington</option>
                                    <option value="Dorton">Dorton</option>
                                    <option value="Edgcott">Edgcott</option>
                                    <option value="Ellesborough">Ellesborough</option>
                                    <option value="Ford">Ford</option>
                                    <option value="Great Horwood">Great Horwood</option>
                                    <option value="Great Kimble">Great Kimble</option>
                                    <option value="Great Missenden">Great Missenden</option>
                                    <option value="Grendon Underwood">Grendon Underwood</option>
                                    <option value="Haddenham">Haddenham</option>
                                    <option value="Halton">Halton</option>
                                    <option value="Hardwick">Hardwick</option>
                                    <option value="Ivinghoe">Ivinghoe</option>
                                    <option value="Kingswood">Kingswood</option>
                                    <option value="Lacey Green">Lacey Green</option>
                                    <option value="Leighton Buzzard">Leighton Buzzard</option>
                                    <option value="Little Horwood">Little Horwood</option>
                                    <option value="Long Crendon">Long Crendon</option>
                                    <option value="Long Marston">Long Marston</option>
                                    <option value="Longwick">Longwick</option>
                                    <option value="Ludgershall">Ludgershall</option>
                                    <option value="Marsh Gibbon">Marsh Gibbon</option>
                                    <option value="Marsworth">Marsworth</option>
                                    <option value="Mentmore">Mentmore</option>
                                    <option value="Middle Claydon">Middle Claydon</option>
                                    <option value="Milton keynes">Milton keynes</option>
                                    <option value="Monks Risborough">Monks Risborough</option>
                                    <option value="Nether Winchendon">Nether Winchendon</option>
                                    <option value="North Marston">North Marston</option>
                                    <option value="Oakley">Oakley</option>
                                    <option value="Oving">Oving</option>
                                    <option value="Pitstone">Pitstone</option>
                                    <option value="Princes Risborough">Princes Risborough</option>
                                    <option value="Shabbington">Shabbington</option>
                                    <option value="Thame">Thame</option>
                                    <option value="Stone">Stone</option>
                                    <option value="Towersey">Towersey</option>
                                    <option value="Tring">Tring</option>
                                    <option value="Waddesdon">Waddesdon</option>
                                    <option value="Weedon">Weedon</option>
                                    <option value="Wendover">Wendover</option>
                                    <option value="Westcott">Westcott</option>
                                    <option value="Weston Turville">Weston Turville</option>
                                    <option value="Whitchurch">Whitchurch</option>
                                    <option value="Worminghall">Worminghall</option>
                                    <option value="Wing">Wing</option>
                                    <option value="Wingrave">Wingrave</option>
                                    <option value="Winslow uk">Winslow</option>
                                    <option value="Wigginton">Wigginton</option>
                                    <option value="Wheatley">Wheatley</option>
                                    <option value="Wotton Underwood">Wotton Underwood</option>
                                    <option value="Steeple Claydon">Steeple Claydon</option>
                                    <option value="Quainton">Quainton</option>
                                    <option value="oxford">oxford</option>
                                    <option value="Boarstall">Boarstall</option>
                                    <option value="Ashendon">Ashendon</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4" style="padding-top: 1%; display: none;" id="booking_home_show">

                            <div class="col-md-9" id="booking_home" style="display: none;" >
                                <textarea style="border-radius: 10px;" class="form-control" name="booking_home_val" id="booking_home_val" cols="18" placeholder="Type complete address.."></textarea>
                            </div>

                        </div>    
                    </div>

                    <div class="col-md-12">
                        <div class="form-group col-md-4" style="padding-top: 1%;">
                            <div class="col-md-3">
                                <label style="color: #000" for="airport">To:</label>
                            </div>
                            <div class="col-md-9">
                                <select style="border-radius: 10px !important;"  name="airport" id="booking_place_to" class="form-control dynamic">
                                    <option value="">--Select--</option>
                                    <option value="to_town">Town/Village</option>
                                    <option value="to_airport">Airport</option>
                                    <option value="to_trainstation">Train Station</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4"  style="padding-top: 1%; display: none;" id="booking_place_show_to">
                            <div class="col-md-1"> </div>

                            <div class="col-md-9" id="booking_trainstation_to" style="display: none;" >
                                <select style="border-radius: 10px !important;" name="booking_trainstation_to" id="booking_trainstation_val_to" class="form-control dynamic">
                                    <option selected="selected" value="">--Select--</option>
                                    <option value="Kings Cross Station">Kings Cross Station</option>
                                    <option value="Marylebone Station">Marylebone Station</option>
                                    <option value="St Pancras Station">St Pancras Station</option>
                                </select>
                            </div>

                            <div class="col-md-9" id="booking_airport_to" style="display: none;" >
                            <select style="border-radius: 10px !important;"  name="booking_airport_to" id="booking_airport_val_to" class="form-control dynamic">
                                <option selected="selected" value="">--Select--</option>
                                <option value="Heathrow Airport Terminal 2">Heathrow Airport Terminal 2</option>
                                <option value="Heathrow Airport Terminal 3">Heathrow Airport Terminal 3</option>
                                <option value="Heathrow Airport Terminal 3">Heathrow Airport Terminal 4</option>
                                <option value="Heathrow Airport Terminal 5">Heathrow Airport Terminal 5</option>
                                <option value="Gatwick Airport South Terminal">Gatwick Airport South Terminal</option>
                                <option value="Gatwick Airport North Terminal">Gatwick Airport North Terminal</option>
                                <option value="Stansted Airport">Stansted Airport</option>
                                <option value="East Midlands Airport">East Midlands Airport</option>
                                <option value="London City Airport">London City Airport</option>
                                <option value="Luton Airport">Luton Airport</option>
                                <option value="Birmingham Airport">Birmingham Airport</option>
                                <option value="Manchester Airport">Manchester Airport</option>
                                <option value="Bristol">Bristol</option>
                            </select>
                            </div>

                            <div class="col-md-9" id="booking_town_to" style="display: none;">
                                <select style="border-radius: 10px !important;" name="booking_town_to" id="booking_town_val_to" class="form-control dynamic" data-dependent="airport">
                                   <option selected="selected" value="">--Select--</option>
                                    <option value="Aylesbury">Aylesbury</option>
                                    <option value="Aston Abbotts">Aston Abbotts</option>
                                    <option value="Aston Clinton">Aston Clinton</option>
                                    <option value="Bicester">Bicester</option>
                                    <option value="Bishopstone">Bishopstone</option>
                                    <option value="Buckingham">Buckingham</option>
                                    <option value="Bierton">Bierton</option>
                                    <option value="Calvert">Calvert</option>
                                    <option value="Chearsley">Chearsley</option>
                                    <option value="Cheddington">Cheddington</option>
                                    <option value="Chilton">Chilton</option>
                                    <option value="Chinnor">Chinnor</option>
                                    <option value="Cuddington">Cuddington</option>
                                    <option value="Dorton">Dorton</option>
                                    <option value="Edgcott">Edgcott</option>
                                    <option value="Ellesborough">Ellesborough</option>
                                    <option value="Ford">Ford</option>
                                    <option value="Great Horwood">Great Horwood</option>
                                    <option value="Great Kimble">Great Kimble</option>
                                    <option value="Great Missenden">Great Missenden</option>
                                    <option value="Grendon Underwood">Grendon Underwood</option>
                                    <option value="Haddenham">Haddenham</option>
                                    <option value="Halton">Halton</option>
                                    <option value="Hardwick">Hardwick</option>
                                    <option value="Ivinghoe">Ivinghoe</option>
                                    <option value="Kingswood">Kingswood</option>
                                    <option value="Lacey Green">Lacey Green</option>
                                    <option value="Leighton Buzzard">Leighton Buzzard</option>
                                    <option value="Little Horwood">Little Horwood</option>
                                    <option value="Long Crendon">Long Crendon</option>
                                    <option value="Long Marston">Long Marston</option>
                                    <option value="Longwick">Longwick</option>
                                    <option value="Ludgershall">Ludgershall</option>
                                    <option value="Marsh Gibbon">Marsh Gibbon</option>
                                    <option value="Marsworth">Marsworth</option>
                                    <option value="Mentmore">Mentmore</option>
                                    <option value="Middle Claydon">Middle Claydon</option>
                                    <option value="Milton keynes">Milton keynes</option>
                                    <option value="Monks Risborough">Monks Risborough</option>
                                    <option value="Nether Winchendon">Nether Winchendon</option>
                                    <option value="North Marston">North Marston</option>
                                    <option value="Oakley">Oakley</option>
                                    <option value="Oving">Oving</option>
                                    <option value="Pitstone">Pitstone</option>
                                    <option value="Princes Risborough">Princes Risborough</option>
                                    <option value="Shabbington">Shabbington</option>
                                    <option value="Thame">Thame</option>
                                    <option value="Stone">Stone</option>
                                    <option value="Towersey">Towersey</option>
                                    <option value="Tring">Tring</option>
                                    <option value="Waddesdon">Waddesdon</option>
                                    <option value="Weedon">Weedon</option>
                                    <option value="Wendover">Wendover</option>
                                    <option value="Westcott">Westcott</option>
                                    <option value="Weston Turville">Weston Turville</option>
                                    <option value="Whitchurch">Whitchurch</option>
                                    <option value="Worminghall">Worminghall</option>
                                    <option value="Wing">Wing</option>
                                    <option value="Wingrave">Wingrave</option>
                                    <option value="Winslow">Winslow</option>
                                    <option value="Wigginton">Wigginton</option>
                                    <option value="Wheatley">Wheatley</option>
                                    <option value="Wotton Underwood">Wotton Underwood</option>
                                    <option value="Steeple Claydon">Steeple Claydon</option>
                                    <option value="Quainton">Quainton</option>
                                    <option value="oxford">oxford</option>
                                    <option value="Boarstall">Boarstall</option>
                                    <option value="Ashendon">Ashendon</option>
                                </select>
                            </div>
                            <div class="col-md-2"> </div>
                        </div>
                        <div class="form-group col-md-4" style="padding-top: 1%; display: none;" id="booking_home_show_to_a">

                            <div class="col-md-9" id="booking_home_to" style="display: none;" >
                                <textarea style="border-radius: 10px;" class="form-control" name="booking_home_val_to" id="booking_home_val_to" cols="10" placeholder="Type complete address.."></textarea>
                            </div>

                        </div>
                    </div>

                    <div style="background: #30b9ee;border-radius: 50px;padding:5px" class="col-md-12">
                        <div class="col-md-12" style="border-radius: 50px;border:1px solid #fff;padding:5px">
                         <label  style="width:30%;color:#fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                            <input checked id="single_journy" value="Single journey" type="radio"  style="width: 100%" name="travel">&nbsp&nbsp Single journey
                            <span class="checkmark" checked="checked" id="single_journy"></span>
                        </label>
                         <label  style="width:30%;color:#fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                            <input id="return_journy" value="return_journey" type="radio" style="width: 100%" name="travel">&nbsp&nbsp Return journey
                            <span class="checkmark" id="return_journy"></span>
                        </label>
                        </div>
                    </div>

                <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;margin-top: 10px;">
                    <h4 style="color:white;margin: 0px">2 Choose your journey date</h4>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" style="background:#f6f6f6;padding: 10px;margin-bottom: 20px">
                        <div class="col-md-12">

                            <div id="single_journy_list">
                                <div class="form-group col-md-12">
                                    <div class="col-md-2">
                                        <label style="color: #000" for="name">Pick Up Time:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" required  type="time" name="pickuptime" id="">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-2">
                                        <label style="color: #000" for="name">Pick up Date:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" required  type="date" name="pickupdate" id="shootdate" min="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>

                            <div style="display: none;" id="return_journy_list">
                                <div class="form-group col-md-12">
                                    <div class="col-md-2">
                                        <label style="color: #000" for="name">Pick-up time to return:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control"  type="time" name="pickuptimereturn" id="">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-2">
                                        <label style="color: #000" for="name">Date of return:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control"  type="date" name="datereturn" id="shootdate" min="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;">
                    <h4 style="color:white;margin: 0px">3 Choose vehicle type</h4>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="background:#f6f6f6;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                        <h6 style="color:white;margin: 0px">Mercedes E Class or Audi A6</h6>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <img style="width: 100%" src="{{asset('arc/images/audi-A6.jpg')}}" alt="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px;" src="{{asset('arc/icons/men.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/bag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/handbag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                                <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                    <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Mercedes E Class or Audi A6">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                        <h6 style="color:white;margin: 0px">Mercedes Benz S Class</h6>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <img style="width: 100%" src="{{asset('arc/images/merc-s-class.jpg')}}" alt="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px;" src="{{asset('arc/icons/men.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/bag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/handbag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                                <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                    <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Mercedes Benz S Class">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                        <h6 style="color:white;margin: 0px">Audi A6 estate ca</h6>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <img style="width: 100%" src="{{asset('arc/images/audi-A6.jpg')}}" alt="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px;" src="{{asset('arc/icons/men.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/bag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/handbag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                                <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                    <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Audi A6 estate ca">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                        <h6 style="color:white;margin: 0px">Mercedes Benz Vito 8 Seater Extra Large</h6>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <img style="width: 100%" src="{{asset('arc/images/vito-8.jpg')}}" alt="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px;" src="{{asset('arc/icons/men.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/bag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/handbag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                                <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                    <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Mercedes Benz Vito 8 Seater Extra Large">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                        <h6 style="color:white;margin: 0px">16 Seater Minibus</h6>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <img style="width: 100%" src="{{asset('arc/images/minibus.png')}}" alt="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px;" src="{{asset('arc/icons/men.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/bag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <img style="margin-top: 20px" src="{{asset('arc/icons/handbag.png')}}" alt="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                                <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                    <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="16 Seater Minibus">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;">
                    <h4 style="color:white;margin: 0px">4 Your details</h4>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="background:#f6f6f6;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-12">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Contact name:</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" required  type="text" name="name" id="name">
                            </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Contact phone number:</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" required  type="text" name="phone" id="name">
                            </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Contact email address:</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" required  type="text" name="email" id="name">
                            </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Number of Passengers:</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" required  type="number" name="passenger" id="name">
                            </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Number of Suitcases:</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" required  type="number" name="suitcase" id="name">
                            </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Number of Hand Luggages:</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" required  type="number" name="luggage" id="name">
                            </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                    <label style="color: #000" for="name">Pick-up Address:</label>
                                </div>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="pickaddress" id="pick_complete" cols="10"></textarea>
                                </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Drop-off Address:</label>
                            </div>
                            <div class="col-md-6">
                                <textarea class="form-control" name="dropaddress" id="drop_complete" cols="15"></textarea>
                            </div>
                        </div><br><hr style="border:1px solid">
                        <div class="form-group col-md-12">
                            <div class="col-md-3">
                                <label style="color: #000" for="name">Any other information:</label>
                            </div>
                            <div class="col-md-6">
                                <textarea class="form-control" name="message" id="" cols="10"></textarea>
                            </div>
                        </div><br><hr>

                        <div style="background: #30b9ee;border-radius: 50px;padding:5px" class="col-md-12">
                            <div class="col-md-12" style="border-radius: 50px;border:1px solid #fff;padding:5px">
                                <h6 style="color:white;text-align:center">In case of Paypal extra commision charged: 2.50 %</h6>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h6 style="text-align:center">
                                <input style="background: #30b9ee;border-radius: 50px;padding:5px;color:white"  type="button"  onclick="calcRoute1();" class="get form-control fix_places" value="Next ">
                                <input style="background: #30b9ee;border-radius: 50px;padding:5px;display: none;color:white"   type="button"  onclick="calcRoute1();" class="get try form-control other_places" value="Next">
                            </h6>
                        </div>
                    </div>

                    <div style="display: none" class="booking-summary" >
                        <h2 class="booking-summary1" style="text-align: center">Booking Summary</h2>
                        <div id="output"></div>

                        <div class="container-fluid booking-summary1">
                            <label  style="float: left;font-size: 15px;" class="hide_stripe radio btn1 contain col-md-12 col-sm-12 col-xs-12">
                                <input type="radio" name="radio" value="cash">&nbspCash Payment
                                <span class="checkmark"></span>
                            </label>
                            <label id="show_stripe"  style="float: left;margin-top:5px;font-size: 15px;" class="btn1 contain col-md-12 col-sm-12 col-xs-12">
                                &nbspCard
                                <input type="radio" name="radio" value="stripe" id="card">
                                <span class="checkmark"></span>
                            </label>
                        </div><br>
                        <div class="form-row">
                            <label for="card-element">
                                Credit or debit card
                            </label>
                            <div id="card-element">
                                <!-- A Stripe Element will be inserted here. -->
                            </div>

                            <!-- Used to display form errors. -->
                            <div id="card-errors" role="alert"></div>
                        </div>
                        <button class="btn btn-primary" id="pay" style="padding: 8px;" type="submit" >Submit</button>
                    </div>

                </form>
            </div>
        </div>
        <div class="container"></div>        
    </section>

    <style>

        .stripe{
            display: none;
        }
        .radio{
            font-size: 20px;
        }
        input[type=text] {
            border-radius: 8px;
        }

        /* The container */
        .contain{
            display: inline;
            cursor: pointer;
            font-size: 12px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default radio button */
        .contain input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom radio button */
        .checkmark {
            position: absolute;
            top: 4px;
            left: 0;
            height: 15px;
            width: 15px;
            padding: 5px;
            background-color: #eee;
            border-radius: 50%;
        }

        /* On mouse-over, add a grey background color */
        .contain:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .contain input:checked ~ .checkmark {
            background-color: #fe5722;
        }
        /*h3{
            background: #3a526a;
            padding: 10px;
            color: white;
            border-top-left-radius: 15px;;
            border-top-right-radius: 15px;;
        }*/
        .radius{
            border-radius: 5px;
        }
        input[type=text] {
            border-radius: 8px;
        }
        input[type=email] {
            border-radius: 8px;
        }

        /* The container */
        .contain{
            display: inline;
            cursor: pointer;
            font-size: 12px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default radio button */
        .contain input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom radio button */
        .checkmark {
            position: absolute;
            top: 4px;
            left: 0;
            height: 15px;
            width: 15px;
            padding: 5px;
            background-color: #eee;
            border-radius: 50%;
        }
        #qourt2{
            border-radius: 5px;
            color: white;margin-top: 5px;
            padding: 5px;
            display: none;
        }

        /* On mouse-over, add a grey background color */
        .contain:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .contain input:checked ~ .checkmark {
            background-color: #03a9f5;
        }
    </style>


    <script src="https://js.stripe.com/v3/"></script>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>

    <script src="https://js.stripe.com/v3/"></script>
    
    <script src="{{ asset('public/js/card.js') }}"></script>

    <script>
        var publishable_key = '{{ env('STRIPE_PUBLISHABLE_KEY') }}';
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            var today = new Date();

            $("#date").datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: today
                // set the minDate to the today's date
                // you can add other options here
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $("#btn1").click(function(){
                $(".pick1").toggle()
            });
            $("#btn").click(function(){
                $(".drop1").toggle();
            });
        });
        $(document).ready(function(){
            $("#journey").click(function(){
                $(".panel1").show();
                $(".panel2").hide();
                $(".panel3").hide();
                $(".get").show();
                $("#qourt2").hide();
                $("#try_again").hide();
            });
            $("#air").click(function(){
                $(".panel1").hide();
                $(".panel2").show();
                $(".panel3").hide();
                $(".get").hide();
                $("#qourt2").show();
                $("#try_again").hide();
            });
            $("#london_tour").click(function(){
                $(".panel1").hide();
                $(".panel2").hide();
                $(".panel3").show();
                $(".get").show();
                $("#try_again").hide();
                $("#qourt2").hide();

            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $("#booking_place").change(function(){
                var x = document.getElementById("booking_place").value;
                $("#booking_place_show").show();
                $("#booking_home_show").show();
                if (x=='from_town') {
                     $("#booking_town").show();
                     $("#booking_home").show();
                     $("#booking_airport").hide();
                     $("#booking_trainstation").hide();
                }else if (x=='from_airport') {
                    $("#booking_airport").show();
                    $("#booking_home").show();
                    $("#booking_town").hide();
                    $("#booking_trainstation").hide();
                } else {
                    $("#booking_trainstation").show();
                    $("#booking_home").show();
                     $("#booking_airport").hide();
                    $("#booking_town").hide();

                }
            });

            $("#booking_place_to").change(function(){
                var x = document.getElementById("booking_place_to").value;
                $("#booking_place_show_to").show();
                $("#booking_home_show_to_a").show();
                if (x=='to_town') {
                     $("#booking_town_to").show();
                     $("#booking_home_to").show();
                     $("#booking_airport_to").hide();
                     $("#booking_trainstation_to").hide();
                }else if (x=='to_airport') {
                    $("#booking_home_to").show();
                    $("#booking_airport_to").show();
                    $("#booking_town_to").hide();
                    $("#booking_trainstation_to").hide();
                } else {
                    $("#booking_home_to").show();
                    $("#booking_trainstation_to").show();
                    $("#booking_airport_to").hide();
                    $("#booking_town_to").hide();
                }
            });


            $("#return_journy").click(function(){
                $("#return_journy_list").show();

            });

            $("#single_journy").click(function(){
                $("#single_journy_list").show();
                $("#return_journy_list").hide();

            });

            $("#booking_town").change(function(){
                var x = document.getElementById("booking_town_val").value;
                $("#pick_complete").html("");
                $('#pick_complete').append(x);
            });

            $("#booking_airport").change(function(){
                var x = document.getElementById("booking_airport_val").value;
                $("#pick_complete").html("");
                $('#pick_complete').append(x);
            });

            $("#booking_trainstation").change(function(){
                var x = document.getElementById("booking_trainstation_val").value;
                $("#pick_complete").html("");
                $('#pick_complete').append(x);
            });

            $("#booking_home").change(function(){
                var x = document.getElementById("booking_home_val").value;
                $("#pick_complete").html("");
                $('#pick_complete').append(x);
            });

            $("#booking_trainstation_to").change(function(){
                var y = document.getElementById("booking_trainstation_val_to").value;
                alert(y);
                $("#drop_complete").html("");
                $('#drop_complete').append(y);
            });

            $("#booking_airport_to").change(function(){
                var y = document.getElementById("booking_airport_val_to").value;
                $("#drop_complete").html("");
                $('#drop_complete').append(y);
            });

            $("#booking_town_to").change(function(){
                var y = document.getElementById("booking_town_val_to").value;
                $("#drop_complete").html("");
                $('#drop_complete').append(y);
            });

            $("#booking_home_to").change(function(){
                var y = document.getElementById("booking_home_val_to").value;
                $("#drop_complete").html("");
                $('#drop_complete').append(y);
            });

            $( function() {
            $( "#shootdate" ).datepicker({
                minDate: 0
             });
             });
        });
    </script>

    <script>
        $(document).ready(function(){
            $(".try").click(function(){
                $("#try_again").show();
            });
        });
        $(document).ready(function(){

            $(".get").click(function(){
                $(this).hide();
                $(".booking-summary").show();
            });
            $("#qourt2").click(function(){
                $(this).hide();
                $(".booking-summary").show();
            });
            $("#showpay").click(function(){
                $("#pay").show();
                $("#ajaxSubmit").hide();
            });
            $("#hidepay").click(function(){
                $("#pay").hide();
                $("#ajaxSubmit").show();

            });

        });
    </script>

    <script>
        $(document).ready(function(){
            $("#show_stripe").click(function () {
                $(".stripe").show();
                $("#showpay").hide();
                $(".hide_stripe").hide();
                $("#pay").show();
                // Create a Stripe client.
                var stripe = Stripe(publishable_key);

                // Create an instance of Elements.
                var elements = stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: '#32325d',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                // Create an instance of the card Element.
                var card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>.
                card.mount('#card-element');

                // Handle real-time validation errors from the card Element.
                card.addEventListener('change', function(event) {
                    var displayError = document.getElementById('card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                    } else {
                        displayError.textContent = '';
                    }
                });

                // Handle form submission.
                var form = document.getElementById('payment-form');
                form.addEventListener('submit', function(event) {
                    event.preventDefault();

                    stripe.createToken(card).then(function(result) {
                        if (result.error) {
                            // Inform the user if there was an error.
                            var errorElement = document.getElementById('card-errors');
                            errorElement.textContent = result.error.message;
                        } else {
                            // Send the token to your server.
                            stripeTokenHandler(result.token);
                        }
                    });
                });

                // Submit the form with the token ID.
                function stripeTokenHandler(token) {
                    // Insert the token ID into the form so it gets submitted to the server
                    var form = document.getElementById('payment-form');
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'stripeToken');
                    hiddenInput.setAttribute('value', token.id);
                    form.appendChild(hiddenInput);

                    // Submit the form
                    form.submit();
                }
            });

            $(".hide_stripe").click(function () {
                $(".form-row").hide();
                $(".stripe").show();
                $("#showpay").hide();
                $("#show_stripe").hide();
                $("#pay").show();
            });
        });
    </script>

    <script type="text/javascript">
        // function abcd() {
            // alert('abcdef');
        // if (document.getElementById("booking_trainstation_val").value) {
        //      var pickairport = document.getElementById("booking_trainstation_val").value;
        //      // alert(pickairport);
        // } else if (document.getElementById("booking_airport_val").value) {
        //     var pickairport = document.getElementById("booking_airport_val").value;
        //     // alert(pickairport);
        // } else if(document.getElementById("booking_town_val").value) {
        //     var pickairport = document.getElementById("booking_town_val").value;
        //     // alert(pickairport);
        // } else {
        //     var pickairport = '';
        //     // alert('from place not found');
        // }
// alert('show from');
// alert(pickairport);
        //saving to val in a variable
        if (document.getElementById("booking_trainstation_val_to").value ) {
             var airport = document.getElementById("booking_trainstation_val_to").value;
             // alert(airport);
        } else if (document.getElementById("booking_airport_val_to").value) {

            var airport = document.getElementById("booking_airport_val_to").value;

        } else if (document.getElementById("booking_town_val_to").value) {
            var airport = document.getElementById("booking_town_val_to").value;
            // alert(airport);
        } else {
            var airport = '';
        }

        // alert(pickairport);
        // alert('show to')
        // alert(airport);
        // alert('endthere');

        if (pickairport!=0 && airport !=0 ) {
            alert('am here');

            $.get("{{URL::to('/read/data/')}}",function(data){
                alert('aaaa');
                // alert(data);
                    $.each(data,function(i,value){
                        alert('bbbb')
                        var id =value.id;
                        var from =value.pickairport;
                        var to = value.airport;
                        var bill = value.bill;

                        if($("#return_journy").is(":checked")) {
                            // alert('in checked')
                                var bill1=Number(bill)+Number(bill);
                                document.getElementById('msg1').innerHTML="Price of meet and greet has beem included";
                                // alert('in if cond')
                                // alert(bill1);
                            }  else {
                                // alert('not checked') 
                                var bill1=bill;
                                // alert(bill1)
                            }

                        // var f_from=pickairport;
                        // var f_to=airport;
                        // alert('f from');
                        // alert(f_from);
                        // alert('to from');
                        // alert(f_to);
                        // alert('from');
                        // alert(from);
                        // alert('to');
                        // alert(to);
                        // alert('bill1');
                        // alert(bill1);
                        // alert('bill');
                        // alert(bill);

                        if(from==f_from && to==f_to)
                        {
                            // alert('from')
                            document.getElementById('newbill').value=bill1;
                            document.getElementById('myText1').value=bill1;
                        }
                    });
                    
                });

            $("#output").html("<div class='row' style='background: #586a79;margin: 0px; padding: 0px;color: white;opacity: 1.7'>" +
//                        "<p class='alert alert-success col-md-6' style='margin: 0px'>"+"Duration: " + result.routes[0].legs[0].duration.text+"</p>" +
                        "<h3 class='alert alert-success  col-md-12' style='background: #586a79;color:white !important;margin: 0px; padding: 0px'>"+"Billing Detail" +"</h3>" +
                        "<p class='alert alert-success  col-md-12' style='background: #586a79;color:white !important;margin: 0px; padding: 0px'>"+"From: " + pickairport+"</p>" +
                        "<p class='alert alert-success   col-md-12' style='margin: 0px;background: #586a79;color:white !important;padding: 0px'>"+"Airport: " + airport +"</p>"  +
                        
                        "<p class='alert alert-success  col-md-12' style='margin: 0px;background: #586a79;color:white !important;padding: 0px' id='newbill'>"+" Cost: " + bill1 +" </p>" +
                        "<p  style='color: white' >"+"<strong>Some Area Prices Are Negotiable<br></strong>"+"</p>" +
                        "<p style='color: white' >"+"<strong><span id='msg1'></span><br></strong>"+"</p>" +
                        "</div>");
        //     alert('cccccc');
        // } else {
        //     $("#output").html("" +
        //         "<div class='alert-danger'><span style='color:red'>Could not retrieve driving distance.</span></div>"
        //     );
        // }
    // }    






        //create request
        // var request1 = {
        //     origin: pickairport,
        //     destination:airport,
        //     travelMode: google.maps.TravelMode.DRIVING, //WALKING, BYCYCLING, TRANSIT
        //     // unitSystem: google.maps.UnitSystem.IMPERIAL
        // }

        // alert(request1);

//         directionsService.route(request, function (result, status) {
//             alert('am here aaaa');
//             if (status == google.maps.DirectionsStatus.OK) {
//                 alert('am here bbbb');
//                 var distance = result.routes[0].legs[0].distance;
//                 var price=distance.value /1609.34;
//                 // alert('price')
//                 // alert(price);
//                 $.get("{{URL::to('/read/data/')}}",function(data){
//                     $.each(data,function(i,value){
//                         var id =value.id;
//                         var from =value.pickairport;
//                         var to = value.airport;
//                         var bill = value.bill;

//                         if($("#return_journy").is(":checked")) {
//                             alert('in checked')
//                                 var bill1=Number(bill)+Number(bill);
//                                 document.getElementById('msg1').innerHTML="Price of meet and greet has beem included";
//                             }  else {
//                                 // alert('not checked') 
//                                 var bill1=bill;
//                                 // alert(bill1)
//                             }

//                         var f_from=pickairport;
//                         var f_to=airport;

//                         if(from==f_from && to==f_to)
//                         {
//                             alert('from')
//                             document.getElementById('newbill').innerHTML=bill1;
//                             document.getElementById('myText1').value=bill1;
//                         }
//                     });
//                 });

//                 //Get distance and time
//                 $("#output").html("<div class='row' style='background: #586a79;margin: 0px; padding: 0px;color: white;opacity: 1.7'>" +
// //                        "<p class='alert alert-success col-md-6' style='margin: 0px'>"+"Duration: " + result.routes[0].legs[0].duration.text+"</p>" +
//                         "<h3 class='alert alert-success  col-md-12' style='background: #586a79;color:white !important;margin: 0px; padding: 0px'>"+"Billing Detail" +"</h3>" +
//                         "<p class='alert alert-success  col-md-12' style='background: #586a79;color:white !important;margin: 0px; padding: 0px'>"+"From: " + pickairport+"</p>" +
//                         "<p class='alert alert-success   col-md-12' style='margin: 0px;background: #586a79;color:white !important;padding: 0px'>"+"Airport: " + airport +"</p>"  +
//                         "<p class='alert alert-success   col-md-12' style='margin: 0px;background: #586a79;color:white !important;padding: 0px'>"+"Driving distance: " + result.routes[0].legs[0].distance.text+"</p>" +
//                         "<p class='alert alert-success  col-md-12' style='margin: 0px;background: #586a79;color:white !important;padding: 0px'>"+" Cost: "+" <i class='fa fa-gbp' aria-hidden='true'></i> " +"<span  id='newbill'>"+"</span>"+"</li>"  +
//                         "<p  style='color: white' >"+"<strong>Some Area Prices Are Negotiable<br></strong>"+"</p>" +
//                         "<p style='color: white' >"+"<strong><span id='msg1'></span><br></strong>"+"</p>" +
//                         "</div>");
//                 $("#map").show();
//                 //display route
//                 directionsDisplay.setDirections(result);
//                 // alert('in if cond')
//             }
//             else {
//                 // alert('in else cond')
//                 //delete route from map
//                 directionsDisplay.setDirections({ routes: [] });
//                 //center map in London
//                 map.setCenter(myLatLng);

//                 //show error message
//                 $("#output").html("" +
//                         "<div class='alert-danger'><span style='color:white'>Could not retrieve driving distance.</span></div>"
//                 );
//             }
//         });
//         } else {
//         $("#output").html("" +
//                         "<div class='alert-danger'><span style='color:red'>Could not retrieve driving distance.</span></div>"
//         );
//         }




        // }
    </script>

@include('calculationz');

@endsection

