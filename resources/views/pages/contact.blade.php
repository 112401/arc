@extends('layout.app')
@section('content')

@push('post-styles')
<style>
    .contact_details{
        margin-top: 10px !important;
    }
</style>
@endpush

<section class="page-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header-title">
                    Contact US
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section" id="">
    <div class="container" id="contact5">
    	<div class="row">
            <div class="col-xs-12 col-md-8 col-lg-8">
                <div class="col-md-6 col-md-offset-3">
                    <!-- @if ($message = Session::get('success')) -->
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <!-- <strong>{{ $message }}</strong> -->
                        </div>
                    <!-- @endif -->
                </div>
                <h3 id="service4"><strong>SEND MESSAGE</strong></h3>
                <form class="contact-form mt-45" action="" method="" id="contact">
                    <!-- IF MAIL SENT SUCCESSFULLY -->
                   <!--  {{csrf_field()}} -->
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 form1">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" id="exampleInputName" placeholder="Name*">
                                @error('name')
                                 <li style="color: red;list-style-type: none">{{ $message }}</li>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12 form1">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                       placeholder="Email*">
                                @error('email')
                                <li style="color: red;list-style-type: none">{{ $message }}</li>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12 form1">
                            <div class="form-group">
                                <input type="text" name="phone" class="form-control"
                                       placeholder="Phone number*">
                                @error('phone')
                                <li style="color: red;list-style-type: none">{{ $message }}</li>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12 form1">
                            <div class="form-group">
                                <input type="text" name="subject" class="form-control"
                                       placeholder="Subject">
                                @error('subject')
                                <li style="color: red;list-style-type: none">{{ $message }}</li>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12 form1">
                            <textarea class="form-control" name="message" rows="12" placeholder="Message*"></textarea>
                            @error('message')
                            <li style="color: red;list-style-type: none">{{ $message }}</li>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8col-md-8 col-xs-12" id="send">
                            <button href="#" class="btn btn-primary hvr-bounce-to-right btn-lg" type="submit" role="button">SEND NOW
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4">
                <h3 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.2s" id="contact"><strong>CONTACT DETAILS</strong></h3>
                <div class="row wow zoomInDown" data-wow-duration="0.5s" data-wow-delay="0.2s">
                    <div class="col-xs-1 col-md-1 col-lg-1">
                        <i class="fa fa-map-marker contact_details"></i>
                    </div>
                    <div class="col-xs-10 col-md-10 col-lg-10">
                        <h4 class="par1"><strong>ADDRESS</strong></h4>
                        <p class="par1">6 Montague Road
                            <br/> Aylesbury
                            Buckinghamshire
                            <br/>HP21 8JT</p>
                    </div>
                </div>
                <div class="row wow zoomInDown" data-wow-duration="0.5s" data-wow-delay="0.4s">
                    <div class="col-xs-1 col-md-1 col-lg-1">
                        <i class="fa fa-phone contact_details"></i>
                    </div>
                    <div class="col-xs-10 col-md-10 col-lg-10">
                        <h4 class="par1"><strong>PHONE</strong></h4>
                        <p class="par1">
                            <a href="tel:01296 393434"> <span style="color: #848484 !important">01296 393434</span></a>
                        </p>
                    </div>
                </div>
                <div class="row  wow zoomInDown" data-wow-duration="0.5s" data-wow-delay="0.6s">
                    <div class="col-xs-1 col-md-1 col-lg-1">
                        <i class="fa fa-inbox contact_details"></i>
                    </div>
                    <div class="col-xs-10 col-md-10 col-lg-10">
                        <h4 class="par1"><strong>EMAIL</strong></h4>
                        <p class="par1">
                            <a href="mailto:info@arcexecutive.co.uk?subject=query about customer">
                                <span style="color: #848484 !important">info@arcexecutive.co.uk</span></a>
                        </p>
                    </div>
                </div>
                <div class="row  wow zoomInDown" data-wow-duration="0.5s" data-wow-delay="0.8s">
                    <div class="col-xs-1 col-md-1 col-lg-1">
                        <i class="fa fa-share-alt contact_details"></i>
                    </div>
                    <div class="col-xs-10 col-md-10 col-lg-10">
                        <h4 class="par1"><strong>SOCIAL MEDIA</strong></h4>
                        <div class="link-box2">
                            <a href="https://www.facebook.com/"><i style="color: #848484 !important " class="twitter fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/"><i style="color: #848484 !important " class="fa fa-instagram twitter"></i></a>
                            <a href="mailto:info@arcexecutive.co.uk?subject=query about customer"><i style="color: #848484 !important " class="fa fa-google">+</i></a>
                        </div>
                        <!-- /.link-box2 -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection