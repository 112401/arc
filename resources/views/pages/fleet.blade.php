 <section class="section" id="clients">
        <div class="container">
            <div class="row">
                <div class="section-title text-center">
                    <span>Our Fleets</span>
                </div>
            </div>
            <div class="row conatnt-row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme" id="clint-logo">

                        <!-- client item -->
                        <div>
                            <div class="logo-wrapper"><img alt="" src="{{asset('arc/images/fleet1.png')}}"></div>
                        </div>
                        <!-- end client item -->

                        <!-- client item -->
                        <div>
                            <div class="logo-wrapper"><img alt="" src="{{asset('arc/images/fleet2.png')}}"></div>
                        </div>
                        <!-- end client item -->

                        <!-- client item -->
                        <div>
                            <div class="logo-wrapper"><img alt="" src="{{asset('arc/images/fleet3.png')}}"></div>
                        </div>
                        <!-- end client item -->

                        <!-- client item -->
                        <div>
                            <div class="logo-wrapper"><img alt="" src="{{asset('arc/images/fleet4.png')}}"></div>
                        </div>
                        <!-- end client item -->

                        <!-- client item -->
                        <div>
                            <div class="logo-wrapper"><img alt="" src="{{asset('arc/images/fleet5.png')}}"></div>
                        </div>
                        <!-- end client item -->

                        <!-- client item -->
                        <div>
                            <div class="logo-wrapper"><img alt="" src="{{asset('arc/images/fleet6.png')}}"></div>
                        </div>
                        <!-- end client item -->

                        <!-- client item -->
                        <div>
                            <div class="logo-wrapper"><img alt="" src="{{asset('arc/images/fleet6.png')}}"></div>
                        </div>
                        <!-- end client item -->

                    </div>
                </div>
            </div>
        </div>
    </section>