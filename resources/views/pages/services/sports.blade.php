@extends('layout.app')
@section('content')

<section class="page-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header-title">
                    SPORT EVENTS
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section" id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <div class="section-title left">
                    <h1>SOCIAL AND SPORTING EVENTS</h1>
                </div>
                <div class="about-contant">
                    <p>It is very important for a person to socialize and to take part in social and sporting events to have a spiffy lifestyle. Whenever your kid has a sport event to attend to, ARC should be your first and only call. We ensure you a safe and pleasureable ride. We make sure you reach the destination safe and sound and on time.</p>
                    
                </div>
            </div>
             <div class="col-md-5 col-sm-12 wow fadeIn">
                <div class="about-right-side">
                    <img class="img-responsive" src="{{asset('arc/images/social.jpg')}}" alt="about-side">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="why-choose ">
        <div class="container ">
            <div class="row ">
                <div class="col-lg-6 col-md-5 col-sm-5 image-section ">
                    <div class="image-cover relative ">
                        <div class="right-absolute wow bounceInDown " data-wow-duration="1s " data-wow-delay="1s ">
                            <img src="{{asset('arc/images/social-sport.jpg')}}" alt="absolute " class="img-responsive " />
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-7 col-sm-7 text-icon ">
                    <h4>ARC EXECUTIVE SOCIAL AND SPORTING EVENTS</h4>
                    <div class="row ">
                        <div class="col-xs-12 col-md-9 col-sm-10 wow bounceInRight " data-wow-duration="1s " data-wow-delay="0.3s ">
                            <p>
                             We provide cars according to your compulsion and specifications. Wether you are going with your friends or family we have a 8seater mini bus specifically for this purpose. Our duty is to make your trip easier, faster and relaxing without you taking any pains of timetables, parking lots etc.</p>
                        </div>
                    </div>
                    <div class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.6s;">
                        <ul class="alert short-list1">
                            <li><i class="fa fa-chevron-right text-primary" aria-hidden="true"></i> O2 Arena</li>
                            <li><i class="fa fa-chevron-right text-success" aria-hidden="true"></i>Wembley Stadium </li>
                            <li> <i class="fa fa-chevron-right text-danger" aria-hidden="true"></i> Wembley arena </li>
                            <li><i class="fa fa-chevron-right text-primary" aria-hidden="true"></i> Twickenham Rugby stadium</li>
                        </ul>
                    </div>
                    <br>
                    <div class="tp-caption grey_heavy_72 skewfromrightshort tp-resizeme rs-parallaxlevel-0" data-x="25" data-y="490" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="3000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12; max-width: inherit; max-height: inherit; white-space: nowrap;">
                        <a href="{{route('arcBookNow')}}" class="btn btn-primary">Book Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection