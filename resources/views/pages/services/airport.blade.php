@extends('layout.app')
@section('content')

<section class="page-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header-title">
                    AIRPORT TRANSFERS
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section" id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <div class="section-title left">
                    <h1>AIRPORT TRANSFERS</h1>
                </div>
                <div class="about-contant">
                    <p>ARC Executive offers you posh airport transportation services that are getable 24*7. we know how exhausting and fatiguing a journey in a plane can be. And trying to find a ride at airport can be hectic any time of the day.</p>
                    <p>Rather than fussing over who is picking you up and brooding over their questionable reliablity, opt for ARC Executive services and wind down and enjoy your journey. We offer you personal meet and greet as well and we can take you from airport to any destination you want. Hire us to enjoy your journey with our luxurious car services in comfort.</p>
                </div>
            </div>
             <div class="col-md-5 col-sm-12 wow fadeIn">
                <div class="about-right-side">
                    <img class="img-responsive" src="{{asset('arc/images/airport-transfer.jpg')}}" alt="about-side">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="why-choose ">
        <div class="container ">
            <div class="row ">
                <div class="col-lg-6 col-md-5 col-sm-5 image-section ">
                    <div class="image-cover relative ">
                        <div class="right-absolute wow bounceInDown " data-wow-duration="1s " data-wow-delay="1s ">
                            <img src="{{asset('arc/images/personal-transfer.jpg')}}" alt="absolute " class="img-responsive " />
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-7 col-sm-7 text-icon ">
                    <h4>PERSONAL TRAVEL</h4>
                    <div class="row ">
                        <div class="col-xs-12 col-md-9 col-sm-10 wow bounceInRight " data-wow-duration="1s " data-wow-delay="0.3s ">
                            <p>
                             ARC preeminent airport services will make any special vacation truly magical and enjoyable from cover to cover for you. Wether you're coming by yourself, family or your friends, we have a car suitable for all your needs and requirements. Leave it to us to serve you with the finest and matchless chauffeur services and enjoy your trip to it's fullest.</p>
                            <h4>GROUP TRAVEL</h4>
                            <p>
                            Larger groups can enjoy our services as well. We have cars available which perfectly serves those flying out of a major corporate event or personal gatherings. Relax and unwind all the niceties you have come to count on.
                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="tp-caption grey_heavy_72 skewfromrightshort tp-resizeme rs-parallaxlevel-0" data-x="25" data-y="490" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="3000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12; max-width: inherit; max-height: inherit; white-space: nowrap;">
                        <a href="{{route('arcBookNow')}}" class="btn btn-primary">Book Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection