@extends('layout.app')
@section('content')

<section class="page-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header-title">
                    WEDDING SERVICES
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section" id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <div class="section-title left">
                    <h1>WEDDING SERVICES</h1>
                </div>
                <div class="about-contant">
                    <p>Getting wedlocked is something every person has dreamt of. No matter how grand or small a wedding is, it holds great sentimental value for both the bride and the groom. Wether traditional or mordern, a wedding day should be as perfect and impeccable as it can be. The soon-to-be bride and groom will love our wedding limo services and their family and friends will equally enjoy our expertly coordinated wedding transportation.</p>
                    
                </div>
            </div>
             <div class="col-md-5 col-sm-12 wow fadeIn">
                <div class="about-right-side">
                    <img class="img-responsive" src="{{asset('arc/images/wedding.jpg')}}" alt="about-side">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="why-choose ">
        <div class="container ">
            <div class="row ">
                <div class="col-lg-6 col-md-5 col-sm-5 image-section ">
                    <div class="image-cover relative ">
                        <div class="right-absolute wow bounceInDown " data-wow-duration="1s " data-wow-delay="1s ">
                            <img src="{{asset('arc/images/wedding-services.jpg')}}" alt="absolute " class="img-responsive " />
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-7 col-sm-7 text-icon ">
                    <h4>ARC EXECUTIVE WEDDING SERVICES</h4>
                    <div class="row ">
                        <div class="col-xs-12 col-md-9 col-sm-10 wow bounceInRight " data-wow-duration="1s " data-wow-delay="0.3s ">
                            <p>
                             ARC Executive promises you a great experience for wedding transportation services. We have extravagant and luxurious cars including Mercedes S class lined up for the bride and groom. There is just no imitation to the style and elegance of the impeccable wedding limousine with a spiffed up chauffeur tagged along. Trust us to make your big day unforgettable and stupendous.</p>
                        </div>
                    </div>
                    <br>
                    <div class="tp-caption grey_heavy_72 skewfromrightshort tp-resizeme rs-parallaxlevel-0" data-x="25" data-y="490" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="3000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12; max-width: inherit; max-height: inherit; white-space: nowrap;">
                        <a href="{{route('arcBookNow')}}" class="btn btn-primary">Book Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection