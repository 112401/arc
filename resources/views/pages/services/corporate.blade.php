@extends('layout.app')
@section('content')

<section class="page-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header-title">
                    CORPORATE & BUSINESS
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section" id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <div class="section-title left">
                    <h1>CORPORATE AND BUSINESS</h1>
                </div>
                <div class="about-contant">
                    <p>Being a business owner can be extremely rewarding but it can be tiring at the same time as one has to attend alot of meetings. As a business owner, YOU are the business. There's a bewildering array of things to worry about that includes attending important meetings on time. Finding a ride that is in accord to your needs and specifications can be a difficult task at times. One needs a standby chauffeur for business meetings and ARC Executive serves the purpose well. Book us for your significant business meetings and Leave the driving to us and retain your energies for the business at hand.</p>
                    
                </div>
            </div>
             <div class="col-md-5 col-sm-12 wow fadeIn">
                <div class="about-right-side">
                    <img class="img-responsive" src="{{asset('arc/images/corporate.jpg')}}" alt="about-side">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="why-choose ">
        <div class="container ">
            <div class="row ">
                <div class="col-lg-6 col-md-5 col-sm-5 image-section ">
                    <div class="image-cover relative ">
                        <div class="right-absolute wow bounceInDown " data-wow-duration="1s " data-wow-delay="1s ">
                            <img src="{{asset('arc/images/corporate and busniess.jpg')}}" alt="absolute " class="img-responsive " />
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-7 col-sm-7 text-icon ">
                    <h4>ARC EXECUTIVE CORPORATE AND BUSINESS</h4>
                    <div class="row ">
                        <div class="col-xs-12 col-md-9 col-sm-10 wow bounceInRight " data-wow-duration="1s " data-wow-delay="0.3s ">
                            <p>
                             We provide exceptional corporate transport services for business meetings. We are highly selective when it comes to opting for drivers and very particular about vetting them for background check and track records. We look for skilled chauffeures to ensure that you have seamless journey with us.</p>
                            <p>We guarantee you that we will excel your needs for sole as well as group corporate transportation needs. Our aim is to proivide our clients with the best chauffeur services at best rates possible.
                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="tp-caption grey_heavy_72 skewfromrightshort tp-resizeme rs-parallaxlevel-0" data-x="25" data-y="490" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="3000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12; max-width: inherit; max-height: inherit; white-space: nowrap;">
                        <a href="{{route('arcBookNow')}}" class="btn btn-primary">Book Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection