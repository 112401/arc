<?php $title='Book Us for Executive Services | PHONE:01296 393434'; ?>
<?php $discription='Book arc executives for affordable airport shuttle services ,chauffeur driven car and wedding cars in Buckinghamshire,
London and Aylebury and Surroundings.'; ?>
<link rel="stylesheet" type="text/css" href="{{asset('public/datepicker')}}/jquery-ui.css">
<script src="{{asset('public/datepicker')}}/jquery-1.12.4.js"></script>
<script src="{{asset('public/datepicker')}}/jquery-ui.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUeFGwxXtr8M8SD82BbA-6857CVPKUhNo&libraries=places"></script>
<script src="http://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>



<script type="text/javascript">
    $(document).ready(function(){

        var today = new Date();

        $("#date").datepicker({
            changeMonth: true,
            changeYear: true,
            minDate: today
            // set the minDate to the today's date
            // you can add other options here
        });
    });
</script>

@extends('app')
@section('main-content')
    <style>

        .stripe{
            display: none;
        }
        .radio{
            font-size: 20px;
        }
        input[type=text] {
            border-radius: 8px;
        }

        /* The container */
        .contain{
            display: inline;
            cursor: pointer;
            font-size: 12px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default radio button */
        .contain input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom radio button */
        .checkmark {
            position: absolute;
            top: 4px;
            left: 0;
            height: 15px;
            width: 15px;
            padding: 5px;
            background-color: #eee;
            border-radius: 50%;
        }

        /* On mouse-over, add a grey background color */
        .contain:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .contain input:checked ~ .checkmark {
            background-color: #fe5722;
        }
        h3{
            background: #3a526a;
            padding: 10px;
            color: white;
            border-top-left-radius: 15px;;
            border-top-right-radius: 15px;;
        }
        .radius{
            border-radius: 5px;
        }
        input[type=text] {
            border-radius: 8px;
        }
        input[type=email] {
            border-radius: 8px;
        }

        /* The container */
        .contain{
            display: inline;
            cursor: pointer;
            font-size: 12px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default radio button */
        .contain input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom radio button */
        .checkmark {
            position: absolute;
            top: 4px;
            left: 0;
            height: 15px;
            width: 15px;
            padding: 5px;
            background-color: #eee;
            border-radius: 50%;
        }
        #qourt2{
            border-radius: 5px;
            color: white;margin-top: 5px;
            padding: 5px;
            display: none;
        }

        /* On mouse-over, add a grey background color */
        .contain:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .contain input:checked ~ .checkmark {
            background-color: #03a9f5;
        }

    </style>
    <script>
        $(document).ready(function(){
            $("#btn1").click(function(){
                $(".pick1").toggle()
            });
            $("#btn").click(function(){
                $(".drop1").toggle();
            });
        });
        $(document).ready(function(){
            $("#journey").click(function(){
                $(".panel1").show();
                $(".panel2").hide();
                $(".panel3").hide();
                $(".get").show();
                $("#qourt2").hide();
                $("#try_again").hide();
            });
            $("#air").click(function(){
                $(".panel1").hide();
                $(".panel2").show();
                $(".panel3").hide();
                $(".get").hide();
                $("#qourt2").show();
                $("#try_again").hide();
            });
            $("#london_tour").click(function(){
                $(".panel1").hide();
                $(".panel2").hide();
                $(".panel3").show();
                $(".get").show();
                $("#try_again").hide();
                $("#qourt2").hide();

            });
        });
    </script>

    <div class="banner-round">
        <div class="tp-banner-container">
            <div class="tp-banner">
                <img style="height: 500px; margin-top: 60px"  src="{{asset('public')}}/images/bookin .jpg" alt="slidebg1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="back-img">
            </div>
        </div>
    </div>
    <!--<div class="container-fluid col-md-12">-->
    <!--    {{--google map--}}-->
    <!--    <div class="container-fluid">-->
    <!--        <div class="col-md-6">-->
                <div id="googleMap" style="height: 400px;display: none">

                </div>
    <!--        </div>-->
    <!--    </div>-->

    <!--</div>-->
    <br>
    <div class="w3-container">
        @if ($message = Session::get('success'))
            <div class="w3-panel w3-green w3-display-container">
                <div class="col-md-8 col-md-offset-3">
                    <p class="alert alert-success" style="margin-top:40px; height: 80px;text-align: center">{{$message}}
                        <button type="button" class="close" data-dismiss="alert">&times</button>
                    </p>
                </div>
            </div>
            <?php Session::forget('success');?>
        @endif
        @if ($message = Session::get('error'))
            <div class="w3-panel w3-green w3-display-container">
                <div class="col-md-8 col-md-offset-3">
                    <p class="alert alert-danger" style="margin-top: -125px;">You can not proceed this process contact with Arc Executive</p>
                </div>
            </div>
            <?php Session::forget('error');?>
        @endif
        @if(Session::has('message'))
            <div class="w3-panel w3-green w3-display-container" role="alert">
                <div class="col-md-8 col-md-offset-3">
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">&times</button>
                        <strong>Warning ! {{ Session::get('message') }}</strong>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <script>
        var publishable_key = '{{ env('STRIPE_PUBLISHABLE_KEY') }}';
    </script>
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}" />
    <script src="https://js.stripe.com/v3/"></script>
    <script src="{{ asset('public/js/card.js') }}"></script>


    <div class="col-md-10 col-md-offset-1" style="position: relative;margin-bottom: 20px">
        <div class="col-md-12 col-sm-12 col-xs-12" style="background: white;border:2px solid #30b9ee;padding: 20px;border-radius: 10px">
            <form method="POST" id="payment-form"  action="{{route('paypal')}}" id="myForm">
                @csrf
                <input required  class="w3-input w3-border" name="amount" id="myText1" type="hidden">
            <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;">
                <h4 style="color:white;margin: 0px">1 Choose your journey</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 " style="background:#f6f6f6;padding: 10px;margin-bottom: 20px">
                <div style="background: #30b9ee;border-radius: 50px;padding:5px" class="col-md-12">
                    <div class="col-md-12" style="border-radius: 50px;border:1px solid #fff;padding:5px">
                        <label  style="width:30%;color:#fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                            <input id="other_places" value="Single journey" type="radio" style="width: 100%" name="check">&nbsp&nbsp Other Places
                            <span class="checkmark"></span>
                        </label>
                        <label  style="width:30%;color:#fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                            <input id="check" value="Return journey" type="radio" style="width: 100%" name="check">&nbsp&nbsp Fix Fare
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <script>
                    jQuery(document).ready(function() {
                        jQuery('#other_places').change(function() {
                            alert('other');
                            if ($(this).prop('checked')) {
                                alert('other checked');
                                $('.other_places').show();
                                $('.fix_places').hide();
                            }
                        });
                        jQuery('#check').change(function() {
                            if ($(this).prop('checked')) {
                                $('.other_places').hide();
                                $('.fix_places').show();
                            }
                        });
                    });
                </script>
                <div class="col-md-12">
                    <div class="form-group col-md-12">
                        <div class="col-md-1">
                            <label style="color: #000" for="name">From:</label>
                        </div>
                        <div class="col-md-6">
                            <select name="pickairport" id="pickairport" class="fix_places form-control dynamic" data-dependent="airport">
                                <option value="">Select pickup</option>
                                @foreach($pickairport_list as $pickairport)
                                    <option value="{{ $pickairport->pickairport}}">{{ $pickairport->pickairport }}</option>
                                @endforeach
                            </select>
                            <div class="form-group">
                                <input type="text" id="from" name="from" placeholder="Enter Pick Up Point" class="form-control radius other_places" style="display: none" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-1">
                            <label style="color: #000" for="name">To:</label>
                        </div>
                        <div class="col-md-6">
                            <select  name="airport" id="airport" class="fix_places form-control dynamic" data-dependent="bill">
                                <option value="">Select Airport</option>
                            </select>
                            <input type="text" name="to" id="to" placeholder="Enter Drop Point" class="form-control radius other_places" style="display: none">
                        </div>
                        {{ csrf_field() }}
                    </div>
                    <div style="background: #30b9ee;border-radius: 50px;padding:5px" class="col-md-12">
                        <div class="col-md-12" style="border-radius: 50px;border:1px solid #fff;padding:5px">
                         <label  style="width:30%;color:#fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                            <input id="return_airport" value="Single journey" type="radio" style="width: 100%" name="travel">&nbsp&nbsp Single journey
                            <span class="checkmark"></span>
                        </label>
                         <label  style="width:30%;color:#fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                            <input id="return_airport" value="Return journey" type="radio" style="width: 100%" name="travel">&nbsp&nbsp Return journey
                            <span class="checkmark"></span>
                        </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;">
                <h4 style="color:white;margin: 0px">2 Choose your journey date</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="background:#f6f6f6;padding: 10px;margin-bottom: 20px">
                <div class="col-md-12">
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label style="color: #000" for="name">Pick Up Time:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="time" name="name" id="name">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label style="color: #000" for="name">Pick up Date:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="date" name="date" id="name">
                        </div>
                    </div>


                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label style="color: #000" for="name">Pick-up time for return:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="time" name="time" id="name">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label style="color: #000" for="name">Date of return:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="date" name="name" id="name">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;">
                <h4 style="color:white;margin: 0px">3 Choose vehicle type</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="background:#f6f6f6;padding: 10px;margin-bottom: 20px">
                <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                    <h6 style="color:white;margin: 0px">Mercedes E Class or Audi A6</h6>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <img style="width: 100%" src="{{asset('public/images/book-car.png')}}" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px;" src="{{asset('public/images/men.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/bag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/handbag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                            <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Mercedes E Class or Audi A6">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                    <h6 style="color:white;margin: 0px">Mercedes Benz S Class</h6>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <img style="width: 100%" src="{{asset('public/images/book-car.png')}}" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px;" src="{{asset('public/images/men.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/bag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/handbag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                            <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Mercedes Benz S Class">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                    <h6 style="color:white;margin: 0px">Audi A6 estate ca</h6>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <img style="width: 100%" src="{{asset('public/images/book-car.png')}}" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px;" src="{{asset('public/images/men.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/bag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/handbag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                            <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Audi A6 estate ca">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                    <h6 style="color:white;margin: 0px">Mercedes Benz Vito 8 Seater Extra Large</h6>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <img style="width: 100%" src="{{asset('public/images/book-car.png')}}" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px;" src="{{asset('public/images/men.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/bag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/handbag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                            <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="Mercedes Benz Vito 8 Seater Extra Large">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                    <h6 style="color:white;margin: 0px">16 Seater Minibus</h6>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <img style="width: 100%" src="{{asset('public/images/book-car.png')}}" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px;" src="{{asset('public/images/men.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/bag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/handbag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                            <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="16 Seater Minibus">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-8" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;;">
                    <h6 style="color:white;margin: 0px">16 Seater Minibus</h6>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #346fc6;background:#fff;padding: 10px;margin-bottom: 20px">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <img style="width: 100%" src="{{asset('public/images/book-car.png')}}" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px;" src="{{asset('public/images/men.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/bag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img style="margin-top: 20px" src="{{asset('public/images/handbag.png')}}" alt="">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <span><b><h4 style="margin-top:30px">x 4</h4></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="col-md-8 col-sm-12 col-xs-12" style="background:#346fc6;float: right;bottom: 0px ">
                            <label  style="width:30%;color: #fff; font-size: 13px;padding: 5px;margin: 15px;" class="radio contain">
                                <input id="journey" type="radio" style="width: 100%" name="vechle_seater" value="16 Seater Minibus">&nbsp&nbsp&nbsp <b><span style='font-size:20px;'>&#163;</span> 0.00</b>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-12 col-sm-12 col-xs-12" style="background: #346fc6; padding: 10px; border-top-left-radius: 15px;  border-top-right-radius: 15px;">
                <h4 style="color:white;margin: 0px">4 Your details</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="background:#f6f6f6;padding: 10px;margin-bottom: 20px">
                <div class="col-md-12">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Contact name:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="text" name="name" id="name">
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Contact phone number:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="text" name="phone" id="name">
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Contact email address:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="text" name="email" id="name">
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Number of Passengers:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="number" name="passenger" id="name">
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Number of Suitcases:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="number" name="suitcase" id="name">
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Number of Hand Luggages:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" required  type="number" name="luggage" id="name">
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Pick-up address:</label>
                        </div>
                        <div class="col-md-6">
                            <textarea class="form-control" name="pick_complete" id="" cols="10"></textarea>
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Drop-off address:</label>
                        </div>
                        <div class="col-md-6">
                            <textarea class="form-control" name="drop_complete" id="" cols="10"></textarea>
                        </div>
                    </div><br><hr style="border:1px solid">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label style="color: #000" for="name">Any other information:</label>
                        </div>
                        <div class="col-md-6">
                            <textarea class="form-control" name="message" id="" cols="10"></textarea>
                        </div>
                    </div><br><hr>

                    <div style="background: #30b9ee;border-radius: 50px;padding:5px" class="col-md-12">
                        <div class="col-md-12" style="border-radius: 50px;border:1px solid #fff;padding:5px">
                            <h6 style="color:white;text-align:center">In case of Paypal extra commision charged: 2.50 %</h6>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h6 style="text-align:center">
                            <input style="background: #30b9ee;border-radius: 50px;padding:5px;color:white"  type="button"  onclick="calcRoute();" class="get form-control fix_places" value="Next ">
                            <input style="background: #30b9ee;border-radius: 50px;padding:5px;display: none;color:white"   type="button"  onclick="Route();" class="get try form-control other_places" value="Next">
                        </h6>
                    </div>
                </div>

                <div style="display: none" class="booking-summary" >
                    <h2 class="booking-summary1" style="text-align: center">Booking Summary</h2>
                    <div id="output" style="padding: 10px;box-shadow:0px 0px 4px 4px #03a9f5; ">

                    </div>

                    <div class="container-fluid booking-summary1">
                        <label  style="float: left;font-size: 15px;" class="hide_stripe radio btn1 contain col-md-12 col-sm-12 col-xs-12">
                            <input type="radio" name="radio" value="cash">&nbspCash Payment
                            <span class="checkmark"></span>
                        </label>
                        {{--<label id="showpay"  style="float: left;font-size: 15px;" class="btn1 contain col-md-12 col-sm-12">--}}
                        {{--&nbspPaypal--}}
                        {{--<input type="radio" name="radio" value="paypal">--}}
                        {{--<span class="checkmark"></span>--}}
                        {{--</label>--}}

                        <label id="show_stripe"  style="float: left;margin-top:5px;font-size: 15px;" class="btn1 contain col-md-12 col-sm-12 col-xs-12">
                            &nbspCard
                            <input type="radio" name="radio" value="stripe" id="card">
                            <span class="checkmark"></span>
                        </label>
                    </div><br>

                    <script src="https://js.stripe.com/v3/"></script>
                    <div class="form-row">
                        <label for="card-element">
                            Credit or debit card
                        </label>
                        <div id="card-element">
                            <!-- A Stripe Element will be inserted here. -->
                        </div>

                        <!-- Used to display form errors. -->
                        <div id="card-errors" role="alert"></div>
                    </div>

                    <script>
                        var publishable_key = '{{ env('STRIPE_PUBLISHABLE_KEY') }}';
                    </script>
                    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}" />
                    <script src="https://js.stripe.com/v3/"></script>
                    <script src="{{ asset('public/js/card.js') }}"></script>


                    <button class="btn btn-primary" id="pay" style="padding: 8px;" type="submit" >Submit</button>
                    {{--<button class="btn btn-primary" style="display: none;padding: 8px" id="ajaxSubmit">Submit stripe</button>--}}
                </div>
            </form>
        </div>
    </div>
    <div class="container ">
        <div class="addd relative">
            <div class="col-lg-6 col-md-6 col-sm-12 call-now w100">
                Call now for
                <br />free consultation <span>:01296 393434</span>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 mail-now w100">
                Mail now for
                <br />free consultation
                <span style="font-size: 15px">:info@arcexecutive.co.uk</span>
            </div>

            <div class="hidden-xs or ">or</div>

        </div>
    </div>
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        $(document).ready(function(){
            $(".try").click(function(){
                $("#try_again").show();
            });
        });
        $(document).ready(function(){

            $(".get").click(function(){
                $(this).hide();
                $(".booking-summary").show();
            });
            $("#qourt2").click(function(){
                $(this).hide();
                $(".booking-summary").show();
            });
            $("#showpay").click(function(){
                $("#pay").show();
                $("#ajaxSubmit").hide();
            });
            $("#hidepay").click(function(){
                $("#pay").hide();
                $("#ajaxSubmit").show();

            });

        });
    </script>
    <script>
        $(document).ready(function(){
            $("#show_stripe").click(function ()
            {
                $(".stripe").show();
                $("#showpay").hide();
                $(".hide_stripe").hide();
                $("#pay").show();
                // Create a Stripe client.
                var stripe = Stripe(publishable_key);

// Create an instance of Elements.
                var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: '#32325d',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

// Create an instance of the card Element.
                var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
                card.mount('#card-element');

// Handle real-time validation errors from the card Element.
                card.addEventListener('change', function(event) {
                    var displayError = document.getElementById('card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                    } else {
                        displayError.textContent = '';
                    }
                });

// Handle form submission.
                var form = document.getElementById('payment-form');
                form.addEventListener('submit', function(event) {
                    event.preventDefault();

                    stripe.createToken(card).then(function(result) {
                        if (result.error) {
                            // Inform the user if there was an error.
                            var errorElement = document.getElementById('card-errors');
                            errorElement.textContent = result.error.message;
                        } else {
                            // Send the token to your server.
                            stripeTokenHandler(result.token);
                        }
                    });
                });

// Submit the form with the token ID.
                function stripeTokenHandler(token) {
                    // Insert the token ID into the form so it gets submitted to the server
                    var form = document.getElementById('payment-form');
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'stripeToken');
                    hiddenInput.setAttribute('value', token.id);
                    form.appendChild(hiddenInput);

                    // Submit the form
                    form.submit();
                }

            });
            $(".hide_stripe").click(function ()
            {
                $(".form-row").hide();
                $(".stripe").show();
                $("#showpay").hide();
                $("#show_stripe").hide();
                $("#pay").show();
            });

        });

    </script>
    @include('js')

@endsection

