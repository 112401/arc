    <footer>
        <div class="container">
            <div class="row">

                <!-- footer column -->
                <div class="col-md-5 col-sm-6 col-xs-12 f-1">
                    <img alt="" class="logo-footer" src="{{asset('arc/images/logo1.png')}}">
                    <p>Arc Executive have amazing and luxurious cars on disposal. We have the car of your dream and you can hire that car on economical and affordable rates...</p>
                    <ul class="social-media">
                        <li>
                            <a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="https://www.pinterest.com/login/"><i class="fa fa-pinterest"></i></a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
                <!-- end footer column -->

                <!-- footer column -->
                <div class="col-md-2 col-sm-6 col-xs-12 f-2">
                    <h2 class="footer-title">Quick Links</h2>
                    <nav>
                        <ul class="footer-list">
                            <li>
                                <a class="page-link-home" href="{{route('arcHome')}}">Home</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcAbout')}}">About Us</a>
                            </li>
                            
                            <li>
                                <a class="page-link" href="{{route('arcService')}}">Services</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcContact')}}">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- end footer column -->

                <!-- footer column -->
                <div class="col-md-2 col-sm-6 col-xs-12 f-3">
                    <h2 class="footer-title"></h2>
                    <nav>
                        <ul class="footer-list">
                            <li>
                                <a class="page-link" href="{{route('arcAirport')}}">Airport</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcCorporate')}}">Corporate</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcCourier')}}">Courier</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcMeetGreet')}}">Meet-Greet</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcSportEvents')}}">Sports-Events</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcLondonTour')}}">London Tours </a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcDayHire')}}">Day Hire</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcWedding')}}">Wedding</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcSeaPort')}}">Seaport</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- end footer column -->

                <!-- footer column -->
                <div class="col-md-3 col-sm-6 col-xs-12 f-4">
                    <h3 class="footer-title">Address</h3>
                    <div class="footer-list">

                        <div class="row-footer">
                            <div class="col-md-4">
                                <p class="abc">6 Montague Road Aylesbury </p> 
                                <p class="abc">Buckinghamshire</p>
                                <p class="abc">  HP21 8JT</p>
                            </div>
                          
                        </div>
                        <div class="row-footer">
                             <div class="col-md-4">
                                <span class="abc">Call :</span>
                                <p class="abc">  01296 393434 </p>
                            </div>
                             <div class="col-md-4">
                                <span class="abc"> Email :</span>
                                <p class="abc"> info@arcexecutive.co.uk </p>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end footer column -->

            </div>
        </div>
    </footer>
    <!--footer End-->

 <!--footer bottom-->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <p class="footer-text-margin">2020 © <a href="https://hybridtechsol.com/" style="color: orange">Hybrid Tech Sol</a>. All rights Reserved</p>
                </div>
            </div>
        </div>
    </div>