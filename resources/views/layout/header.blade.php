    <!--header start--> 
    <div class="header-fix"></div>
    <header class="header-dark">
        <div class="navbar navbar-default yamm container">
            <!-- navbar-header -->
            <div class="navbar-header">
                <!-- menu Toggle -->
                <button class="navbar-toggle" data-target="#navbar-collapse-grid" data-toggle="collapse" type="button"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <ul>
                    <li class="search-open hide-md">
                        <!-- serachbar Toggle for mobile only -->
                        <a href="#"><i class="fa fa-search"></i></a>
                    </li>
                    <li>
                        <!--company logo-->
                         <a class="navbar-brand" href="{{route('arcHome')}}"><img alt="logo" src="{{('arc/images/logo.png')}}"></a>
                    </li>
                </ul>
            </div><!--end navbar-header -->

            <!--Navigation Start-->
            <nav class="navbar-collapse collapse" id="navbar-collapse-grid">
                <ul class="nav navbar-nav navbar-right" id="mainNav"> 
                    <li>
                        <a class="page-link" href="{{route('arcHome')}}">Home</a>
                    </li>
                    <li>
                        <a class="page-link" href="{{route('arcAbout')}}">About</a>
                    </li>
                    <li class="dropdown" style="width: 14%">
                        <a  aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" onclick="window.location.href = '{{route('arcService')}}';" role="button">Services</a>
                        <ul style="margin: 0px" id="t_item" class="dropdown-menu col-1">
                            <li>
                                <a class="page-link" href="{{route('arcAirport')}}">Airport</a>
                            </li>

                            <li>
                                <a class="page-link" href="{{route('arcCorporate')}}">Corporate </a>
                            </li>

                            <li>
                                <a class="page-link" href="{{route('arcCourier')}}">Courier </a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcMeetGreet')}}">Meet-Greet</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcSportEvents')}}">Sports-Events</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcLondonTour')}}">London Tours</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcDayHire')}}">Day Hire</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcWedding')}}">Wedding</a>
                            </li>
                            <li>
                                <a class="page-link" href="{{route('arcSeaPort')}}">Seaport</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="page-link" href="{{route('arcContact')}}">Contact Us</a>
                    </li>
                    
                    <li class="sign-in-open">
                        <form action="{{route('arcBookNow')}}">
                            <button class="btn btn-primary" style="margin-top: 21px !important">Book now</button>
                        <!-- <a href="{{route('arcBookNow')}}" class="btn btn-primary">Book Now</a> -->
                        </form>
                    </li>
                </ul>
            </nav>
            <!--Navigation end-->

            <!--serchbar start-->
            <div class="search-bar">
                <form>
                    <input name="search" placeholder="Type Here" type="text"> <input class="search-btn" type="submit" value="Search">
                </form>
            </div>
            <!--End Start-->

        </div>
    </header>
    <!--Header End-->