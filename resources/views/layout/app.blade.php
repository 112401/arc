<!-- 
developed by Mudassir Ali
dated 27 july 2020 to 12 aug 2020 
 -->
<!DOCTYPE html>
<html lang="">
    
<head>
        <title>arcexecutive</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <meta name="description" content="Arcexecutive provide transport services">
        <meta name="_token" content="{{csrf_token()}}" />

        <!-- fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
        <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/icofont.css')}}" rel="stylesheet">

        <!-- animation -->
        <link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet">
        <!-- owl carousel -->
        <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
        <!-- bootstrap-->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- style css -->
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
        <script type="text/javascript">
            var BASE_URL = "{{ url('/')}}";
        </script>
         @stack('post-styles')

    </head>

    <body>

    <!-- site loader -->
    <div class="loader"></div>
    <!-- end site loader -->

    @section('header')
    @include('layout.header')
    @show

    @yield('content')

    <!--footer Start-->
    @section('footer')
    @include('layout.footer')
    @show
    <!-- end footer bottom-->


    <!--back to to -->
    <a class="BackToTop" href="#"><i class="fa fa-angle-up"></i></a>

    @stack('pre-scripts')
    @section('scripts')

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap-min-js -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- waypoints-min-js -->
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!-- counterup-min-js -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!-- jquery.touchSwipe js -->
    <script src="{{asset('assets/js/jquery.touchSwipe.min.js')}}"></script>
    <!-- wow Animation js -->
    <script src="{{asset('assets/js/wow.js')}}"></script>
    <!-- owl carasol js -->
    <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
    <!--smoth-scroll -->
    <script src="{{asset('assets/js/smoth-scroll.js')}}"></script>
    <!-- this is only for demo purpose replace with new one -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2xWvnzKWfFoa8rRoM8fyAwCKRq03BwaA&amp;sensor=SET_TO_TRUE_OR_FALSE"></script>

    <!-- validate js -->
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
    <!-- map-js -->
    <script src="{{asset('assets/js/map.js')}}"></script>
    <!-- custom js -->
    <script src="{{asset('assets/js/bear.js')}}"></script>

    <script type="text/javascript">
        $(function() {
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>

    <script type="text/javascript">
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
    </script>
    @show
        
    @stack('post-script') 

    </body>
</html>