@extends('layout.app')
@section('content')
    <!-- slider start -->
    <section class="carousel slide slider-main" id="Carousel">

        <!-- slider Indicators -->
        <ol class="carousel-indicators">
            <li class="active" data-slide-to="0" data-target="#Carousel"></li>
            <li data-slide-to="1" data-target="#Carousel"></li>
            <li data-slide-to="2" data-target="#Carousel"></li>
        </ol>
        <!-- slider Indicators -->

        <!-- carousel-inner -->
        <div class="carousel-inner">

            <!-- slide 1 -->
            <div class="item active" style="background-image:url(arc/images/slider1.jpg);">
                <div class="slide-overlay"></div>
                <div class="carousel-caption slider-text">
                    <div class="container">
                        <h1 class="animated fadeInLeft">Book & pay online &nbsp</h1>
                        <p class="animated fadeInUp">Business Meetings</p>
                        <!-- <p class="animated fadeInUp">Special Occasions</p> -->
                        <a class="" href="{{route('arcContact')}}">Contact Us</a>
                    </div>
                </div>
            </div>
            <!-- end slide 1 -->

            <!-- slide 2 -->
            <div class="item" style="background-image:url(arc/images/slider2.jpg);">
                <div class="slide-overlay"></div>
                <div class="carousel-caption slider-text">
                    <div class="container">
                        <h1 class="animated fadeInLeft">Luxury Executive Travel</h1>
                        <p class="animated fadeInUp">Airport Transfers</p>
                        <p class="animated fadeInUp">Weddings</p>
                        <a class="" href="{{route('arcContact')}}">Contact Us</a>
                    </div>
                </div>
            </div>
            <!-- slide 2 -->

            <!-- slide 3 -->
            <div class="item" style="background-image:url(arc/images/slider3.jpg);">
                <div class="slide-overlay"></div>
                <div class="carousel-caption slider-text">
                    <div class="container">
                        <h1 class="animated fadeInLeft">Personal chauffeur</h1>
                        <p class="animated fadeInUp">Sporting Events</p>
                        <p class="animated fadeInUp">Sightseeing</p>
                        <a class="" href="{{route('arcContact')}}">Contact Us</a>
                    </div>
                </div>
            </div>
            <!-- slide 3 -->
        </div>

        <!-- slider controls -->
        <!-- <a class="left carousel-control" data-slide="prev" href="#Carousel">
            <span class="icon-prev">
                <img alt="" src="assets/images/pre.svg">
            </span>
        </a> -->
        <!-- <a class="right carousel-control" data-slide="next" href="#Carousel">
            <span class="icon-next">
                <img alt="" src="assets/images/next.svg">
            </span>
        </a> -->
        <!-- end slider controls -->
    </section>
    <!--Slider End-->


    <!--About Us Start-->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <!-- aboutus content -->
                <div class="col-md-7 col-sm-12">
                    <div class="section-title left">
                        <span>About Us</span>
                        <h1></h1>
                    </div>
                    <div class="about-contant">
                        <p>Welcome to the new Airport Executive Website - Airport Transfers & Chauffeur Driven Cars in Aylesbury Airport Transfers Aylesbury We have been providing executive airport transfers and chauffeur driven cars in Aylesbury for over 15 years and are one of the Aylesbury's leading private hire firms. Whether you are coming to Aylesbury on business or pleasure, by yourself or with family and friends, we have a car to suit your needs.</p>
                        <p>Executive has been in the car Hire services for 15 years. We have amazing reputation in the market because we have always provided Quality Services to all out customers. .  We are experts in business transport and chauffeuring services. </p>
                        <a class="link" href="{{route('arcAbout')}}">Read More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <!-- end aboutus content -->

                <!-- about right image -->
                <div class="col-md-5 col-sm-12 wow fadeIn">
                    <div class="about-right-side">
                        <img class="img-responsive" src="{{asset('arc/images/about-us.jpg')}}" alt="about-side">
                    </div>
                </div>
                <!-- about right image -->
            </div>
        </div>
        <!-- decoration text -->
      <!--   <div class="decoration-text">
             Executive
        </div> -->
        <!-- end decoration text -->
    </section>
    <!--About End-->

    <!--Services Start-->
    <section class="section bg-grey" id="services">
        <div class="container">
            <div class="row">
                <div class="section-title text-center">
                    <span>Our Services</span>
                    <h1>We Provide World Class Services</h1>
                </div>
            </div>
            <div class="row conatnt-row">
                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp">
                    <div class="service-box-1">
                        <span><i class="fa fa-plane"></i></span>
                        <a href="{{route('arcAirport')}}">AIRPORT</a>
                        <p>we know how exhausting and fatiguing a journey in a plane can be...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="service-box-1">
                        <span><i class="fa fa-building-o"></i></span>
                        <a href="{{route('arcCorporate')}}">CORPORATE</a>
                        <p>Finding a ride that is in accord to your needs and specifications can be a difficult...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="service-box-1">
                        <span><i class="fa fa-truck"></i></span>
                        <a href="{{route('arcCourier')}}">COURIER SERVICES</a>
                        <p>ARC Executive is proud to familairize their clients with their competitive courier services...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="service-box-1">
                        <span><i class="fa fa-handshake-o"></i></span>
                        <a href="{{route('arcMeetGreet')}}">MEET AND GREET</a>
                        <p>ARC Executive provides optimum and competetive meet and greet services in town...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="service-box-1">
                        <span><i class="fa fa-futbol-o"></i></span>
                        <a href="{{route('arcSportEvents')}}">SOCIAL AND SPORTING EVENTS</a>
                        <p>We ensure you a safe ride. We make sure you reach the destination safe and sound...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="service-box-1">
                        <span><i class="fa fa-tripadvisor"></i></span>
                        <a href="{{route('arcLondonTour')}}">LONDON TOURS</a>
                        <p>Bet on ARC Executive for a hassle free journey and revel in your trip to it's fullest...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="service-box-1">
                        <span><i class="fa fa-tasks"></i></span>
                        <a href="{{route('arcDayHire')}}">DAY HIRE </a>
                        <p> Our day hire service ables you to use our luscious car services for all day long...</p>
                    </div>
                </div>
                <!-- end service-box -->

                <!-- service-box -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.7s">
                    <div class="service-box-1 last">
                        <span><i class="fa fa-heart"></i></span>
                        <a href="{{route('arcWedding')}}">WEDDING SERVICES</a>
                        <p>The soon-to-be bride and groom will love our wedding limo services will enjoy our coordinated wedding transportation...</p>
                    </div>
                </div>
                <!-- end service-box -->
            </div>
        </div>
    </section>
    <!--Services End-->

    <!-- choose us start -->
    <section class="two-box-section bg-dark">
        <div class="container-fluid">
            <div class="row equal-height">
                <div class="col-md-6">
                    <div class="box-bg" style="background-image: url(arc/images/choose-us.jpg);"></div>
                </div>
                <div class="col-md-6">
                    <div class="box-contant">
                        <h2>WHY CHOOSE US</h2>
                        <p>ARC Executive serves you with striking executive travel services for Buckinghamshire. ARC Executive provides you 24-hour service all over the London and is your reliable source of chauffeured transportation. We believe a client's time is their most important asset. The proficient chauffeures at ARC Executive clicnches that you reach your destination timely and safely.</p>
                        <p>You can choose Arc Executive for your transport services. We are offering</p>
                        <ul class="box-listing">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>24 Hour Online Support</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Dedicated Team</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Our online booking is always available.</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Luxurious cars</a></li>  
                        </ul>
                        <a class="btn btn-theme margin-t-10" href="{{route('arcBookNow')}}">Book Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- choose us end -->

    <!-- travel services start-->
    <section class="counter" id="fun-facts" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-3 col-sm-3">
                    <h3 style="border-bottom: 2px solid #70b6e5;">
                        EXCEPTIONAL TRAVEL SERVICES
                    </h3>
                </div>
                <div class="col-md-12 col-md-9 col-sm-9" >
                <p style="color: black;">ARC Executive serves you with striking executive travel services for Buckinghamshire. ARC Executive provides you 24-hour service all over the London and is your reliable source of chauffeured transportation. We believe a client's time is their most important asset. The proficient chauffeures at ARC Executive clicnches that you reach your destination timely and safely.</p>
                <p style="color: black;">Our chauffeures have local knowledge and they are always complaisant and well behaved.They are thoroughly catechized for skills and expertise so our clients have a great deal of experience with us. Reservations can be made seamlessly online or with one of our professional reservation agents 24\7.</p>
                </div>

            </div>
        </div>
    </section>
    <!-- travel services end -->

    <!-- Happy client start -->
    <section class="section bg-grey" id="Testimonial">
        <div class="container">
            <div class="row">
                <div class="section-title text-center">
                    <span>HAPPY CLIENTS</span>
                </div>
            </div>
            <div class="row conatnt-row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="owl-carousel owl-theme" id="client-tell">

                        <!-- item -->
                        <div class="item">
                            <div class="client-contant">
                                <p>I remember Hiring Arc executive for airport transfers and I must say that they have amazing services. I enjoyed my journey a lot.</p>
                                <div class="client-picture"><img alt="" src="{{asset('assets/images/testimonial-1.jpg')}}"></div>
                                    <h3 class="client-name">Mary Gonzalez</h3>
                            </div>
                        </div>
                        <!-- end item -->

                        <!-- item -->
                        <div class="item">
                            <div class="client-contant">
                                <p>My Kid had a Sporting event and I hired Arc executive for this purpose.  must say that I am exceptionally happy with their prompt services. My kids enjoyed the luxurious cars and the services of their amazing chauffeurs.</p>
                                <div class="client-picture"><img alt="" src="{{asset('assets/images/testimonial-2.jpg')}}"></div>
                                <h3 class="client-name">Selena Herrera</h3>
                            </div>
                        </div>
                        <!-- end item -->

                        <!-- item -->
                        <div class="item">
                            <div class="client-contant">
                                <p>Arc executive is a team of professionals. I really enjoyed my journey with them  They are exceptional and they are very prompt.</p>
                                <div class="client-picture"><img alt="" src="{{asset('assets/images/testimonial-3.jpg')}}"></div>
                                <h3 class="client-name">Joyce Christ</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- client end -->

    <!--Fleet Start-->
    @include('pages.fleet');
    <!--Fleet Start-->


    <!--Call To Action Start-->
  <!--   <section class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h1>powerful single page template</h1>
                    <a class="btn btn-md" href=""><i aria-hidden="true" class="fa fa-shopping-cart"></i> Book Now </a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section> -->
    <!--Call To Action Start-->
    @endsection
