<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\place;
use DB;

class PagesController extends Controller{

    public function index(){
        return view('ajaxget');
    }

    public function getUsers($id = 0) {
        // Fetch all records
        $userData['data'] = Page::getuserData($id);

        echo json_encode($userData);
        exit;
    }
    public function readdata(){
        // Fetch all records
        $students = place::get();

        // dd($students);
        

        return response($students);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'from'=>'required',
            'to'=>'required',
            'bill'=>'required',
        ]);
        $booking=new place();
        $booking->pickairport=$request->from;
        $booking->airport=$request->to;
        $booking->bill=$request->bill;
        $booking->save();
        return redirect()->back()->with('messag', 'booking submited! soon Contact with you');

    }
    public function view()
    {

        $pickairport_list = DB::table('places')->groupBy('pickairport')->get();
        return view('index')->with('pickairport_list', $pickairport_list);
    }
    public function booking() {

        $pickairport_list =DB::table('places')->groupBy('pickairport')->get();
        return view('pages.booknow')->with('pickairport_list', $pickairport_list);

    }
    public  function stripe()
    {
        return view('stripe');
    }

    public  function poststripe(Request $req){
        // Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey("sk_test_TWLvt9AC5YFuydwngTXzY47F009rJwM1Ts");

// Token is created using Checkout or Elements!
// Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        $charge = \Stripe\Charge::create([
//            'name' => $req->name,
            'amount' => $req->payment*100,
//            'email' => $req->email,
            'currency' => 'usd',
            'description' => 'Example charge',
            'source' => $token,
        ]);
        dd('task sucessful');

    }
}
