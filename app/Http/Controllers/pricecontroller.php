<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\biling;

class pricecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bill=biling::all();
        return view('admin.fair_detail',compact('bill'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'price1'=>'required',
            'price2'=>'required',
            'price3'=>'required',
            'price4'=>'required',
        ]);
        $bill=new biling();
        $bill->price1=$request->price1;
        $bill->price2=$request->price2;
        $bill->price3=$request->price3;
        $bill->price4=$request->price4;
        $bill->save();
        return redirect()->back()->with('message', 'Prices has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bill=biling::where('id',$id)->first();
        return view('admin.edit_fair_detail',compact('bill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

//        $this->validate($request,[
//            'price1'=>'required',
//            'price2'=>'required',
//            'price3'=>'required',
//            'price4'=>'required',
//            'price5'=>'required',
//            'price6'=>'required',
//            'price7'=>'required',
//            'price8'=>'required',
//            'price9'=>'required',
//            'price10'=>'required',
//            'price11'=>'required',
//        ]);
        $bill=biling::find($id);
        $bill->price1=$request->price1;
        $bill->price2=$request->price2;
        $bill->price3=$request->price3;
        $bill->price4=$request->price4;
        $bill->price5=$request->price5;
        $bill->price6=$request->price6;
        $bill->price7=$request->price7;
        $bill->price8=$request->price8;
        $bill->price9=$request->price9;
        $bill->price10=$request->price10;
        $bill->price11=$request->price11;

        $bill->save();
        return redirect()->route('price.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        biling::where('id',$id)->delete();
        return redirect()->back();
    }
}

