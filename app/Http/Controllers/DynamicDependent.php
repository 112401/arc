<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\places;
use DB;

class DynamicDependent extends Controller
{
    function index()
    {
        $pickairport_list = DB::table('places')
            ->groupBy('pickairport')
            ->get();
        return view('dynamic_dependent')->with('pickairport_list', $pickairport_list);
    }

    function fetch(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = DB::table('places')
            ->where($select, $value)
            ->groupBy($dependent)
            ->get();
        $output = '<option value="">Select '.ucfirst($dependent).'</option>';
        foreach($data as $row)
        {
            $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
        }
        echo $output;
    }
}
