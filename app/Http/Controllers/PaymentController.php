<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;

use Omnipay\Omnipay;
use App\Payment;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use Validator;
use URL;

use Illuminate\Support\Facades\Mail;
//use App\Mail\sendmail;
use App\Mail\bookingmail;
use App\Groceries;

class PaymentController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }
    public function index()
    {
        return view('paywithpaypal');
    }
    public function paywithpaypal(Request $request)
    {

        // dd($request);


        if($request->radio=='cash')
        {
            // dd($request->amount);
            if($request->amount=='NaN')
            {
                \Session::put('error', 'You can not proceed this process because of amount');
                return Redirect::to('/bookings');
            }
            elseif(empty($request->amount))
            {
                \Session::put('error', 'You can not proceed this process');
                return Redirect::to('/bookings');
            }

            $data = array(
                    'name'           =>  $request->name,
                    'email'          =>  $request->email,
                    'phone'          =>  $request->phone    ,
                    'from'           =>  $request->pickaddress,
                    'to'		     =>	 $request->dropaddress,
                    'flight_number'	 =>  $request->flight_number,
                    'vechle_seater'	 =>  $request->vechle_seater,
                    'date'		     =>	 $request->pickupdate,
                    'time'           =>  $request->pickuptime,
                    'passenger'      =>  $request->passenger,
                    'luggage'        =>  $request->luggage,
                    'suitcase'       =>  $request->suitcase,
                    'travel'         =>  $request->travel,
                    'myText1'        =>  $request->amount,
                    'payment_type'   =>  'Cash By stripe',
                    'message'        =>  $request->otherinfo,
                    'pick1'      => $request->pick_complete,
                    'pick2'      =>  $request->pick2,
                    'pick3'      =>  $request->pick3,
                    'drop1'     =>$request->drop_complete1,
                    'drop2'     =>$request->drop2,
                    'drop3'     =>$request->drop3,

            );
// dd($request);

// dd($request);

            // Mail::to('hurichuhan@gmail.com')->send(new BookingMail($data));
//        return back()->with('success', 'Thanks for your booking!');

            if ($request->booking_trainstation1 || $request->booking_airport1 || $request->booking_town1  ) {
                if ($request->booking_trainstation1) {
                    $a = $request->booking_trainstation1;
                } elseif ($request->booking_airport1) {
                    $a = $request->booking_airport1;
                } else {
                    $a = $request->booking_town1;
                }
            }

            if ($request->booking_trainstation_to || $request->booking_airport_to || $request->booking_town_to  ) {
                if ($request->booking_trainstation_to) {
                    $b = $request->booking_trainstation_to;
                } elseif ($request->booking_airport_to) {
                    $b = $request->booking_airport_to;
                } else {
                    $b = $request->booking_town_to;
                }
            }

            $grocery = new Groceries();
            $grocery->payment_type = "Cash By hand";
            $grocery->cost = $request->amount;
            $grocery->from = $a.' '.$request->booking_home_val;
            $grocery->to = $b.' '.$request->booking_home_val_to;
            $grocery->Travel_Type = $request->travel;
            $grocery->date = $request->pickupdate;
            $grocery->time = $request->pickuptime;
            $grocery->time_return = $request->pickuptimereturn;
            $grocery->date_return = $request->datereturn;
            $grocery->vechle_seater = $request->vechle_seater;

            $grocery->name = $request->name;
            $grocery->email = $request->email;
            $grocery->phone = $request->phone;
            $grocery->passenger = $request->passenger;
            $grocery->suitcase = $request->suitcase;
            $grocery->luggage = $request->luggage;
            $grocery->message = $request->message;

            $grocery->save();
            return back()->with('success', 'Thanks for your booking!');

        }
        elseif($request->radio=='paypal'){
            Session::put('form', $request->from);
            Session::put('pickairport', $request->pickairport);
            Session::put('pickup', $request->pickup);
            Session::put('london_tour', $request->london_tour);
            Session::put('pick1', $request->pick1);
            Session::put('pick2', $request->pick2);
            Session::put('pick3', $request->pick3);
            Session::put('to', $request->to);
            Session::put('drop1', $request->drop1);
            Session::put('drop2', $request->drop2);
            Session::put('drop3', $request->drop3);

            Session::put('name', $request->name);
            Session::put('email', $request->email);
            Session::put('phone', $request->phone);
            Session::put('airport', $request->airport);
            Session::put('flight_number', $request->flight_number);
            Session::put('passenger', $request->passenger);
            Session::put('suitcase', $request->suitcase);
            Session::put('luggage', $request->luggage);
            Session::put('vechle_seater', $request->vechle_seater);
            Session::put('date', $request->date);
            Session::put('time', $request->time);
            Session::put('message', $request->message);
            Session::put('myText1', $request->amount);
            Session::put('travel', $request->travel);
            $myText1= $request->session()->get('myText1');
            if($myText1=='NaN')
            {
                \Session::put('error', 'You can not proceed this process');
                return Redirect::to('/bookings');
            }
            elseif(empty($myText1))
            {
                \Session::put('error', 'You can not proceed this process');
                return Redirect::to('/bookings');
            }
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $item_1 = new Item();

            $item_1->setName('Item 1') /** item name **/
            ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($request->get('amount')); /** unit price **/

            $item_list = new ItemList();
            $item_list->setItems(array($item_1));

            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setTotal($request->get('amount'));

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
            ->setCancelUrl(URL::to('status'));

            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/
            try {

                $payment->create($this->_api_context);

            } catch (\PayPal\Exception\PPConnectionException $ex) {

                if (\Config::get('app.debug')) {

                    \Session::put('error', 'Connection timeout');
                    return Redirect::to('/');

                } else {

                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::to('/');

                }

            }

            foreach ($payment->getLinks() as $link) {

                if ($link->getRel() == 'approval_url') {

                    $redirect_url = $link->getHref();
                    break;

                }

            }

            /** add payment ID to session **/
            Session::put('paypal_payment_id', $payment->getId());

            if (isset($redirect_url)) {

                /** redirect to paypal **/
                return Redirect::away($redirect_url);

            }

            \Session::put('error', 'Unknown error occurred');
            return Redirect::to('/');

        }
        elseif($request->radio=='stripe')
        {
            if($request->amount=='NaN')
            {
                \Session::put('error', 'You can not proceed this process');
                return Redirect::to('/bookings');
            }
            elseif(empty($request->amount))
            {
                \Session::put('error', 'You can not proceed this process');
                return Redirect::to('/bookings');
            }
            
            if ($request->input('stripeToken')) {

                $gateway = Omnipay::create('Stripe');
                $gateway->setApiKey(env('STRIPE_SECRET_KEY'));

                $token = $request->input('stripeToken');

                $response = $gateway->purchase([
                    'amount' => $request->input('amount'),
                    'currency' => env('STRIPE_CURRENCY'),
                    'token' => $token,
                ])->send();

                if ($response->isSuccessful())
                {
                    // payment was successful: insert transaction data into the database
                    $arr_payment_data = $response->getData();

                    $isPaymentExist = Payment::where('payment_id', $arr_payment_data['id'])->first();

                    if(!$isPaymentExist)
                    {
                        $data = array(
                            'name'       =>  $request->name,
                            'email'      =>  $request->email,
                            'phone'      =>  $request->phone,
                            'from'       =>  $request->from.$request->from1.$request->pickairport,
                            'pick1'      =>  $request->pick_complete,
                            'pick2'      =>  $request->pick2,
                            'pick3'      =>  $request->pick3,
                            'to'		 =>	 $request->to.$request->airport,
                            'drop1'		 =>  $request->drop_complete1,
                            'drop2'		 =>  $request->drop2,
                            'drop3'		 =>  $request->drop3,
                            'flight_number'	=>	$request->flight_number,
                            'vechle_seater'	=>	$request->vechle_seater,
                            'date'		  => $request->date,
                            'time'        =>  $request->time,
                            'passenger'   =>  $request->passenger,
                            'luggage'     =>  $request->luggage,
                            'suitcase'    =>  $request->suitcase,
                            'travel'      =>  $request->travel,
                            'myText1'     =>  $request->amount,
                            'payment_type'=> 'Cash By stripe',
                            'message'     =>  $request->message,

                        );

                        Mail::to('hurichuhan@gmail.com')->send(new BookingMail($data));
//        return back()->with('success', 'Thanks for your booking!');

                        $grocery = new Grocery();
                        $grocery->from = $request->from.$request->from1.$request->pickairport.",".$request->pick1.",".$request->pick2.",".$request->pick3;
                        $grocery->to = $request->to.$request->airport.",".$request->drop1.",".$request->drop2.",".$request->drop3;
                        $grocery->name = $request->name;
                        $grocery->email = $request->email;
                        $grocery->phone = $request->phone;
                        $grocery->flight_number = $request->flight_number;
                        $grocery->passenger = $request->passenger;
                        $grocery->suitcase = $request->suitcase;
                        $grocery->luggage = $request->luggage;
                        $grocery->vechle_seater = $request->vechle_seater;
                        $grocery->date = $request->date;
                        $grocery->time = $request->time;

                        $grocery->payment_type = "Cash By Stripe";
                        $grocery->cost = $request->amount;
                        $grocery->Travel_Type = $request->travel;

                        $grocery->message = $request->message;

                        $grocery->save();
                        return back()->with('success', 'Thanks for your booking!');

                    }

                }
                else
                {
                     $msg=$response->getMessage();
                    return back()->with('message',$msg);
                  
                }
            }
        }
        else{
                return back()->with('message','please select payment method Cash by hand or stripe');
        }
    }

    public function getPaymentStatus(Request $request)
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            \Session::put('error', 'Payment failed');
            return Redirect::to('/');

        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved')
        {

            $from = $request->session()->get('form');
            $pickairport = $request->session()->get('pickairport');
            $pickup = $request->session()->get('pickup');
            $london_tour = $request->session()->get('london_tour');
            $pick1 = $request->session()->get('pick1');
            $pick2 = $request->session()->get('pick2');
            $pick3 = $request->session()->get('pick3');
            $to= $request->session()->get('to');
            $drop1= $request->session()->get('drop1');
            $drop2= $request->session()->get('drop2');
            $drop3= $request->session()->get('drop3');

            $name= $request->session()->get('name');
            $email= $request->session()->get('email');
            $phone= $request->session()->get('phone');
            $airport= $request->session()->get('airport');
            $flight_number= $request->session()->get('flight_number');
            $passenger= $request->session()->get('passenger');
            $suitcase= $request->session()->get('suitcase');
            $luggage= $request->session()->get('luggage');
            $vechle_seater= $request->session()->get('vechle_seater');
            $date= $request->session()->get('date');
            $time= $request->session()->get('time');
            $myText1= $request->session()->get('myText1');
            $message= $request->session()->get('message');
            $travel= $request->session()->get('travel');
            $data = array(
                'name' =>$name,
                'email' =>$email,
                'phone' =>$phone,
                'from' =>$from.$pickairport,
                'pick1' =>$pick1,
                'pick2' =>$pick2,
                'pick3' =>$pick3,
                'to' => $to.$airport,
                'drop1' =>$drop1,
                'drop2' =>$drop2,
                'drop3' =>$drop3,
                'flight_number' =>$flight_number,
                'vechle_seater' => $vechle_seater,
                'date' => $date,
                'time' => $time,
                'passenger' => $passenger,
                'luggage' =>$luggage,
                'suitcase' =>$suitcase,
                'message' => $message,
                'payment_type' => 'pay by paypal',
                'myText1'   =>   $myText1,
                'travel'   =>   $travel,
            );

            Mail::to('hurichuhan@gmail.com')->send(new BookingMail($data));
            $grocery = new Grocery();
            $grocery->from = $from." , ".$pickairport.", ".$pick1." , ".$pick2." , ".$pick3;
            $grocery->to = $to.", ".$airport.", "." , ".$drop1." , ".$drop2." , ".$drop3;
            $grocery->name = $name;
            $grocery->email = $email;
            $grocery->phone = $phone;
            $grocery->flight_number = $flight_number;

            $grocery->passenger = $passenger;
            $grocery->suitcase = $suitcase;
            $grocery->luggage = $luggage;

            $grocery->vechle_seater = $vechle_seater;
            $grocery->date = $date;
            $grocery->time = $time;
            $grocery->Travel_Type = $travel;
            $grocery->payment_type = "pay by paypal";
            $grocery->cost =$myText1;
            $grocery->message = $message;


            $grocery->save();

            return back()->with('success','Thanks for Booking!');

            \Session::put('success', 'Payment success');
            return Redirect::to('/');

        }

        \Session::put('error', 'Payment failed');
        return Redirect::to('/');

    }

}
