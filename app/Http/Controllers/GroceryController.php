<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
//use App\Mail\sendmail;
use App\Mail\bookingmail;
use App\Grocery;
use App\biling;
use Validator;
use App\places;

class GroceryController extends Controller
{
    public function store(Request $request)
    {

        $data = array(
            'name'      =>  $request->name,
            'email'      =>  $request->email,
            'phone'      =>  $request->phone,
            'from'      =>   $request->from.$request->from1.$request->pickup,
            'pick1'      =>  $request->pick1,
            'pick2'      =>  $request->pick2,
            'pick3'      =>  $request->pick3,
            'to'		=>	$request->to.$request->airport.$request->london_tour,
            'drop1'		=>$request->drop1,
            'drop2'		=>$request->drop2,
            'drop3'		=>$request->drop3,
            'airport'		=>	$request->airport,
            'flight_number'		=>	$request->flight_number,
            'vechle_seater'		=>	$request->vechle_seater,
            'date'		=>	$request->date,
            'time'   =>   $request->time,
            'passenger'      =>  $request->passenger,
            'luggage'      =>  $request->luggage,
            'suitcase'      =>  $request->suitcase,
            'travel'   =>   $request->travel,
            'myText1'   =>   $request->myText1,
            'payment_type'   => 'Cash By Hand',
            'message'      =>  $request->message,


        );

        Mail::to('hurichuhan@gmail.com')->send(new BookingMail($data));
//        return back()->with('success', 'Thanks for your booking!');

        $grocery = new Grocery();
        $grocery->from = $request->from.$request->from1.$request->pickup.",".$request->pick1.",".$request->pick2.",".$request->pick3;
        $grocery->to = $request->to.$request->airport.$request->london_tour.",".$request->drop1.",".$request->drop2.",".$request->drop3;
        $grocery->name = $request->name;
        $grocery->email = $request->email;
        $grocery->phone = $request->phone;
        $grocery->airport = $request->airport;
        $grocery->flight_number = $request->flight_number;
        $grocery->passenger = $request->passenger;
        $grocery->suitcase = $request->suitcase;
        $grocery->luggage = $request->luggage;
        $grocery->vechle_seater = $request->vechle_seater;
        $grocery->date = $request->date;
        $grocery->time = $request->time;

        $grocery->payment_type = "Cash By hand";
        $grocery->cost = $request->myText1;
        $grocery->Travel_Type = $request->travel;

        $grocery->message = $request->message;

        $grocery->save();
        return back()->with('success', 'Thanks for your booking!');
    }
    public function index_aylsbary()
    {
        $bill=biling::first();
        return view('admin.Aylesbury_airport_price',compact('bill'));
    }
    public function edit_aylsbary($id)
    {
        $bill=biling::where('id',$id)->first();
        return view('admin.edit_fair_aylesbury',compact('bill'));
    }
    public function update_aylsbary(Request $request, $id)
    {

        $this->validate($request,[
            'airport1'=>'required',
            'airport2'=>'required',
            'airport3'=>'required',
            'airport4'=>'required',
            'airport5'=>'required',
            'airport6'=>'required',
            'airport7'=>'required',
            'airport8'=>'required',
            'airport9'=>'required',
            'airport10'=>'required',
            'airport11'=>'required',
            'airport12'=>'required',
        ]);
        $bill=biling::find($id);
        $bill->airport1=$request->airport1;
        $bill->airport2=$request->airport2;
        $bill->airport3=$request->airport3;
        $bill->airport4=$request->airport4;
        $bill->airport5=$request->airport5;
        $bill->airport6=$request->airport6;
        $bill->airport7=$request->airport7;
        $bill->airport8=$request->airport8;
        $bill->airport9=$request->airport9;
        $bill->airport10=$request->airport10;
        $bill->airport11=$request->airport11;
        $bill->airport12=$request->airport12;
        $bill->save();
        return redirect()->route('airport_price');
    }

    public function edit_aston_abbotts($id)
    {
        $places=places::where('id',$id)->first();
        return view('admin.edit-places',compact('places'));
    }
    public function update_aston_abbotts(Request $request, $id)
    {

        $this->validate($request,[
            'from'=>'required',
            'to'=>'required',
            'bill'=>'required',
        ]);
        $bill=places::find($id);
        $bill->pickairport=$request->from;
        $bill->airport=$request->to;
        $bill->bill=$request->bill;
        $bill->save();
        return redirect()->route('aston_clinton_airport_price');
    }
    public function index_aston_abbotts()
    {
        $places=places::all();
        return view('admin.aston_clinton_airport_price',compact('places'));
    }
    public function destroy($id)
    {
        places::where('id',$id)->delete();
        return redirect()->back();
    }
}

