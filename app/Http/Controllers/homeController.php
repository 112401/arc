<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
	public function home() {
    	return view('layout.home');
    }

    public function about() {
    	return view('pages.about');
    }

     public function services() {
    	return view('pages.service');
    }

     public function contact() {
    	return view('pages.contact');
    }

    public function booking() {
        return view('pages.bookNow');
    }

     public function airport() {
    	return view('pages.services.airport');
    }

     public function corporate() {
    	return view('pages.services.corporate');
    }

     public function courier() {
    	return view('pages.services.courier');
    }

     public function meetGreet() {
    	return view('pages.services.meetgreet');
    }

    public function sportEvents() {
    	return view('pages.services.sports');
    }

    public function londonTour() {
    	return view('pages.services.londontour');
    }

    public function dayHire() {
    	return view('pages.services.dayhire');
    }

    public function wedding() {
    	return view('pages.services.wedding');
    }

    public function seaPort() {
    	return view('pages.services.seaport');
    }
}
