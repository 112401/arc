<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendmail;
use App\Mail\bookingmail;
use App\User;
use App\biling;
use App\booking;
use App\places;

class mailcontroller extends Controller
{
    public function send()
    {
    Mail::send(new sendmail());
    }
    function email(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'email'  =>  'required',
            'phone'  =>  'required',
            'subject'  =>  'required',
            'message' =>  'required'
        ]);

        $data = array(
            'name'      =>  $request->name,
            'email'		=>	$request->email,
            'phone'		=>	$request->phone,
            'subject'		=>	$request->subject,
            'message'   =>   $request->message

        );

        Mail::to('info@arcexecutive.co.uk')->send(new SendMail($data));
        return back()->with('success', 'Thanks for contacting us!');

    }
    public function getFare()
    {
        $getFare=biling::orderBy('created_at','DESC')->first();
        return json_encode($getFare);
    }
    public function getplaces()
    {
        $getplaces=places::all();
        return response()->json($getplaces);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'from'=>'required',
            'date'=>'required',
            'time'=>'required',
            'travel'=>'required',
        ]);
        $data = array(
            'name'      =>  $request->name,
            'email'      =>  $request->email,
            'phone'      =>  $request->phone,
            'from'      =>  $request->from.",".$request->pick1,
            'to'		=>	$request->to.",".$request->drop1,
            'airport'		=>	$request->airport,
            'flight_number'		=>	$request->flight_number,
            'vechle_seater'		=>	$request->vechle_seater,
            'date'		=>	$request->date,
            'time'   =>   $request->time,
            'passenger'      =>  $request->passenger,
            'luggage'      =>  $request->luggage,
            'suitcase'      =>  $request->suitcase,
            'travel'   =>   $request->travel,
            'message'      =>  $request->message,

        );

        Mail::to('hurichuhan@gmail.com')->send(new BookingMail($data));
//        return back()->with('success', 'Thanks for your booking!');

        $booking=new booking();
        $booking->name=$request->name;
        $booking->email=$request->email;
        $booking->phone=$request->phone;
        $booking->from=$request->from.",".$request->pick1;
        $booking->airport=$request->airport;
        $booking->to=$request->to.",".$request->drop1;
        $booking->flight_number=$request->flight_number;
        $booking->vechle_seater=$request->vechle_seater;
        $booking->passenger=$request->passenger;
        $booking->suitcase=$request->suitcase;
        $booking->luggage=$request->luggage;
        $booking->date=$request->date;
        $booking->time=$request->time;
        $booking->message=$request->message;
        $booking->travel=$request->travel;
        $booking->save();
        return redirect()->back()->with('message', 'booking submited! soon Contact with you');
        return $booking;
    }

}
