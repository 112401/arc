<?php

namespace App;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class pages extends Model
{
    public static function getuserData($id=0){

        if($id==0){
            $value=DB::table('places')->orderBy('id', 'asc')->get();
        }else{
            $value=DB::table('places')->where('id', $id)->first();
        }
        return $value;

    }
}
