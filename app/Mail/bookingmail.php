<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class bookingmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $order =new booking();
    if($request->airport!=null)
    {
     $order='Airport Booking';
    }
      else
      {
          $order='New Booking';
      }
        return $this->from('bookings@kmlcars.co.uk',$order)
            ->subject('New response')->view('dynamically_booking_email')
            ->with('data', $this->data);
    }
}
