<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.about');
// });

   //stripe route
Route::get('/stripe', 'PagesController@stripe')->name('stripe');
Route::post('/poststripe', 'PagesController@poststripe')->name('poststripe');


Route::get('/abc', 'PagesController@index'); // localhost:8000/
Route::view('/slider', 'slider'); // localhost:8000/
Route::get('/read/data/', 'PagesController@readdata'); // localhost:8000/
Route::get('/getUsers/{id}','PagesController@getUsers');

Route::get('/app', function () {
    return view('app');
});
Route::get('/','PagesController@view')->name('index');
Route::get('/bookings','PagesController@booking')->name('bookings');



Route::get('/','homeController@home')->name('arcHome');
Route::get('about-us','homeController@about')->name('arcAbout');
Route::get('services','homeController@services')->name('arcService');
Route::get('contact-us','homeController@contact')->name('arcContact');
Route::get('book-Now','PagesController@booking')->name('arcBookNow');
Route::get('service/airport','homeController@airport')->name('arcAirport');
Route::get('service/corporate','homeController@corporate')->name('arcCorporate');
Route::get('service/courier','homeController@courier')->name('arcCourier');
Route::get('service/meet-greet','homeController@meetGreet')->name('arcMeetGreet');
Route::get('service/sports-events','homeController@sportEvents')->name('arcSportEvents');
Route::get('service/london-tour','homeController@londonTour')->name('arcLondonTour');
Route::get('service/dayhire','homeController@dayHire')->name('arcDayHire');
Route::get('service/wedding','homeController@wedding')->name('arcWedding');
Route::get('service/seaport','homeController@seaPort')->name('arcSeaPort');

Route::post('/form-submit', 'PaymentController@paywithpaypal')->name('paypal');
Route::post('/form-submit10', 'PaymentController@paywithpaypal')->name('paypal10');
Route::get('/status', 'PaymentController@getPaymentStatus')->name('status');
Route::post('/charge', 'PaymentController@paywithpaypal')->name('pay');

Route::post('/grocery/post', 'GroceryController@store');

   //  fix places
// Route::view('/add-places','admin.add-places')->middleware('auth')->name('add');
Route::get('/all_places', 'GroceryController@index_aston_abbotts')->middleware('auth')->name('aston_clinton_airport_price');
Route::get('/edit-place/{id}', 'GroceryController@edit_aston_abbotts')->middleware('auth')->name('aston_abbotts.edit');
Route::put('update-place/{id}', 'GroceryController@update_aston_abbotts')->middleware('auth')->name('update.aston_abbotts');
Route::delete('delete-place/{id}', 'GroceryController@destroy')->middleware('auth')->name('delete.aston_clinton');

Route::get('/dynamic_dependent', 'DynamicDependent@index');
Route::post('dynamic_dependent/fetch', 'DynamicDependent@fetch')->name('dynamicdependent.fetch');

Route::POST('/booked','mailcontroller@store')->name('booked');
Route::POST('/send/email','mailcontroller@email')->name('sendmail');
Route::get('/sendemail/send', 'mailcontroller@send');
Route::post('/getFare', 'mailcontroller@getFare')->name('getFare');
Route::post('/getplaces', 'mailcontroller@getplaces')->name('getplaces');

Route::resource('/admin/price', 'pricecontroller')->middleware('auth');


